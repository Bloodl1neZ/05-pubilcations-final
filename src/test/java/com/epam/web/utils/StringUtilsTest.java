package com.epam.web.utils;

import org.junit.Assert;
import org.junit.Test;

public class StringUtilsTest {
    private static final String MONEY_STRING = "22.34";
    private static final String EMPTY_STRING = "  ";
    private static final String MONEY_REGEX = "^[0-9]+((\\.|,)[0-9]{1,2})?$";

    @Test
    public void moneyNotNullableShouldMatchToRegEx() {
        Assert.assertTrue(StringUtils.isMatchesToRegEx(MONEY_STRING, MONEY_REGEX, false));
    }

    @Test
    public void moneyNullableShouldMatchToRegEx() {
        Assert.assertTrue(StringUtils.isMatchesToRegEx(MONEY_STRING, MONEY_REGEX, false));
    }

    @Test
    public void emptyStringNullableShouldMatchToRegEx() {
        Assert.assertTrue(StringUtils.isMatchesToRegEx(EMPTY_STRING, MONEY_REGEX, true));
    }

    @Test
    public void emptyStringNotNullableShouldNotMatchToRegEx() {
        Assert.assertFalse(StringUtils.isMatchesToRegEx(EMPTY_STRING, MONEY_REGEX, false));
    }
}
