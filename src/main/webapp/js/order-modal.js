function openModal() {
    var editModal = document.getElementById("pay-modal");
    editModal.style.display = "block";
    document.body.style.overflow = 'hidden';
}
// Get the modal
var editModal = document.getElementById("pay-modal");

// Get the <span> element that closes the modal
var editSpan = document.getElementsByClassName("edit-close")[0];

// When the user clicks on <span> (x), close the modal
editSpan.onclick = function() {
    editModal.style.display = "none";
    document.body.style.overflow = 'auto';

}


// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == editModal) {
        editModal.style.display = "none";
        document.body.style.overflow = 'auto';
    }
}