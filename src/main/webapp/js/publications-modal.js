function setValuesInModal(id, title, publisherId, costPerMonth, type) {
    document.forms["edit-publication-form"]["id"].value = id;
    document.forms["edit-publication-form"]["title"].value = title;
    document.forms["edit-publication-form"]["publisher"].value = publisherId;
    document.forms["edit-publication-form"]["cost-per-month"].value = costPerMonth;
    var editRadio = document.querySelector('.edit-input input[value=' + type + ']');
    if (editRadio) {
        editRadio.setAttribute('checked', true);
    }
    var editModal = document.getElementById("edit-modal");
    editModal.style.display = "block";

    document.body.style.overflow = 'hidden';
}

// Get the modal
var editModal = document.getElementById("edit-modal");

// Get the <span> element that closes the modal
var editSpan = document.getElementsByClassName("edit-close")[0];

// When the user clicks on <span> (x), close the modal
editSpan.onclick = function() {
    editModal.style.display = "none";
    document.body.style.overflow = 'auto';

}


// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == editModal) {
        editModal.style.display = "none";
        document.body.style.overflow = 'auto';
    }
}
