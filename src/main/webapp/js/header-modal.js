
function openModal() {
    var modal = document.getElementById("header-left");
    var hambO = document.getElementById("hamburger-open");
    var hambC = document.getElementById("hamburger-close");
    modal.style.display = 'block';
    hambO.style.display = 'none';
    hambC.style.display = 'block';
    document.body.style.overflow = 'hidden';
}
function closeModal() {
    var modal = document.getElementById("header-left");
    var hambO = document.getElementById("hamburger-open");
    var hambC = document.getElementById("hamburger-close");
    modal.style.display = 'none';
    hambO.style.display = 'block';
    hambC.style.display = 'none';
    document.body.style.overflow = 'auto';
}