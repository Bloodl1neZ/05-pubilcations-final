<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<title>Main page</title>
<body>
    <jsp:forward page="WEB-INF/pages/main.jsp"></jsp:forward>
</body>
</html>
