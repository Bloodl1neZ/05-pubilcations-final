<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 11/30/2018
  Time: 7:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page import="com.epam.web.entity.PublicationType" %>
<c:set var="language" value="${not empty param.language ? param.language : (not empty language ? language : pageContext.request.locale)}" scope="session"/>


<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="pagecontent" var="bundle"/>

<html>
<head>
    <title><fmt:message key="main.main-page" bundle="${bundle}"/></title>
</head>
<body>
    <div class="all-wrapper">
        <jsp:include page="../fragments/header.jsp"/>
        <section class="main-section full-width">
            <div class="main-section-content">
                <div class="text-info">
                    <h1><fmt:message key="main.choose-publication-type" bundle="${bundle}"/></h1>
                </div>
                <jsp:include page="../fragments/search.jsp"/>
                <div class="all-publication-types clearfix">
                    <c:forEach items="${PublicationType.values()}" var="publicationType">
                        <div class="publication-type-block">
                            <a href="${pageContext.servletContext.contextPath}/controller?command=show_publications_by_type&publication-type=${publicationType.value}" class="publication-button">
                                <p><fmt:message key="publication-type.${publicationType.value}" bundle="${bundle}"/></p>
                                <img src="../img/${publicationType.value}.svg" alt="${publicationType.value}">
                            </a>
                        </div>
                    </c:forEach>
                </div>
                <div class="show-all-block">
                    <a href="${pageContext.servletContext.contextPath}/controller?command=show_all_publications" class="show-all-button"><fmt:message key="main.show-all-publications" bundle="${bundle}"/></a>
                </div>
            </div>
        </section>
    </div>
</body>
</html>
