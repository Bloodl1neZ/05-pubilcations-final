<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 12/24/2018
  Time: 2:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page import="com.epam.web.utils.MonthUtils" %>

<c:set var="language" value="${not empty param.language ? param.language : (not empty language ? language : pageContext.request.locale)}" scope="session"/>

<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="pagecontent" var="bundle"/>

<html>
<head>
    <title><fmt:message key="order.order" bundle="${bundle}"/></title>
</head>
<body>
    <div class="all-wrapper">
        <jsp:include page="../fragments/header.jsp"/>
        <section class="order clearfix">
            <div class="text-content">
                <c:choose>
                    <c:when test="${orderDto.order.id == currentOrder.id}">
                        <h2><fmt:message key="order.current-order" bundle="${bundle}"/></h2>
                    </c:when>
                    <c:otherwise>
                        <h2><fmt:message key="order.order" bundle="${bundle}"/> #${orderDto.order.id}</h2>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="order-history-wrapper page-part">
                <p><fmt:message key="order.order-history" bundle="${bundle}"/></p>
                <c:forEach items="${paidOrders}" var="paidOrder">
                    <c:if test="${!(paidOrder == orderDto.order)}">
                        <div>
                            <a href="${pageContext.servletContext.contextPath}/controller?command=go_order_page_by_order_id&id=${paidOrder.id}"><fmt:message key="order.order" bundle="${bundle}"/> #${paidOrder.id}</a>
                        </div>
                    </c:if>
                </c:forEach>
            </div>
            <div class="order-wrapper page-part">
                <div class="order-block">
                    <c:if test="${orderDto.subscriptionDtos.isEmpty()}">
                        <p><fmt:message key="order.empty" bundle="${bundle}"/></p>
                    </c:if>
                    <c:forEach items="${orderDto.subscriptionDtos}" var="subscriptionDto">
                        <c:set var="publication" value="${subscriptionDto.publicationDto.publication}"/>
                        <c:set var="publisher" value="${subscriptionDto.publicationDto.publisher}"/>
                        <c:set var="subscription" value="${subscriptionDto.subscription}"/>
                        <div class="subscription">
                            <div class="subscription-block clearfix">
                                <div class="img-block subscription-part">
                                    <img src="../img/${publication.publicationType.value}.svg" alt="${publication.publicationType.value}">
                                </div>
                                <div class="publication-info-block subscription-part">
                                    <p class="info"><fmt:message key="order.publication" bundle="${bundle}"/></p>
                                    <p class="publication-info-text">${publication.title}</p>
                                    <p class="publication-info-text">${publisher.company}</p>
                                    <p class="publication-info-text"><fmt:message key="publication-type.${publication.publicationType.value}" bundle="${bundle}"/></p>
                                </div>
                                <div class="cost-per-month-block subscription-part">
                                    <p class="info"><fmt:message key="order.cost-per-month" bundle="${bundle}"/></p>
                                    <p>${publication.costPerMonth}$</p>
                                </div>
                                <div class="month-select-block subscription-part">
                                    <p class="info"><fmt:message key="order.month-amount" bundle="${bundle}"/></p>
                                    <c:choose>
                                        <c:when test="${orderDto.order == currentOrder}">
                                            <form action="${pageContext.servletContext.contextPath}/controller?command=change_month_amount_in_subscription" method="post">
                                                <input type="hidden" name="subscription-id" value="${subscription.id}" pattern="^[0-9]+$">
                                                <select name="month-amount" size="1">
                                                    <c:forEach items="${MonthUtils.AVAILABLE_MONTHS}" var="month">
                                                        <option <c:if test="${subscription.monthAmount == month}">selected</c:if> value="${month}">
                                                            ${month} <fmt:message key="subscription.month-${month}" bundle="${bundle}"/>
                                                        </option>
                                                    </c:forEach>
                                                </select>
                                                <input type="submit" value="<fmt:message key="order.save-changes" bundle="${bundle}"/>">
                                            </form>
                                        </c:when>
                                        <c:otherwise>
                                            <p>${subscription.monthAmount}  <fmt:message key="subscription.month-${subscription.monthAmount}" bundle="${bundle}"/></p>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <div class="total-cost-for-subscription subscription-part">
                                    <p class="info"><fmt:message key="order.total-cost" bundle="${bundle}"/></p>
                                    <p>${subscriptionDto.calculateCost()}$</p>
                                </div>
                                <c:if test="${orderDto.order == currentOrder}">
                                    <div class="delete-subscription subscription-part">
                                        <form action="${pageContext.servletContext.contextPath}/controller?command=delete_subscription_by_id" method="post">
                                            <input type="hidden" value="${subscription.id}" name="id">
                                            <input type="submit" value="&times;" class="save-button">
                                        </form>
                                    </div>
                                </c:if>
                            </div>

                        </div>
                    </c:forEach>
                    <c:if test="${!orderDto.subscriptionDtos.isEmpty()}">
                        <div class="buy-all-block">
                            <p class="total-cost"><fmt:message key="order.all-total" bundle="${bundle}"/>: <span class="price">${orderDto.calculateTotalCost()}$</span></p>
                            <c:if test="${orderDto.order == currentOrder}">
                                <button class="order-button" id="open-modal-button" onclick="
                                        openModal()">
                                    <fmt:message key="order.to-order" bundle="${bundle}"/>
                                </button>
                            </c:if>
                        </div>
                    </c:if>
                </div>
            </div>
        </section>
        <c:if test="${currentOrder == orderDto.order}">
            <div id="pay-modal" class="modal">
                <!-- Modal content -->
                <div class="modal-content">
                    <span class="edit-close close">&times;</span>
                    <form name="edit-publication-form" action="${pageContext.servletContext.contextPath}/controller?command=pay_for_order" method="post">
                        <div class="text-edit">
                            <p><fmt:message key="order.card-num" bundle="${bundle}"/></p>
                            <input required type="text" name="number" pattern="^[0-9]{16}$">
                        </div>
                        <div class="text-edit">
                            <p><fmt:message key="order.name" bundle="${bundle}"/></p>
                            <input required type="text" name="name" pattern="^[A-Z ,.'-]+$">
                        </div>
                        <div class="pay-button">
                            <input type="submit" value="<fmt:message key="order.pay" bundle="${bundle}"/>">
                        </div>
                    </form>
                </div>
            </div>
        </c:if>
    </div>
</body>
<script src="${pageContext.request.contextPath}/js/order-modal.js"></script>
</html>
