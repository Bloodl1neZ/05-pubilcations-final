<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 1/12/2019
  Time: 11:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="UTF-8" isELIgnored="false" %>
<%@ page import="com.epam.web.entity.PublicationType" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:set var="language" value="${not empty param.language ? param.language : (not empty language ? language : pageContext.request.locale)}" scope="session"/>

<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="pagecontent" var="bundle"/>

<html>
<head>
    <title><fmt:message key="publication.publication-manager" bundle="${bundle}"/></title>
</head>
<body>
    <div class="all-wrapper">
        <jsp:include page="../fragments/header.jsp"/>
        <section class="publication-manager">
            <div class="publication-manager-block">
                <div class="text-info">
                    <h3><fmt:message key="publication.add-publication-title" bundle="${bundle}"/></h3>
                </div>
                <div class="add-form-wrapper">
                    <form name="add-publication-form" action="${pageContext.servletContext.contextPath}/controller?command=add_publication" method="post">
                        <div class="field-block input-block">
                            <p><fmt:message key="publication.title" bundle="${bundle}"/></p>
                            <input required type="text" name="title" pattern="^[A-Za-z0-9',. ]{1,40}$">
                        </div>
                        <div class="field-block select-block">
                            <p><fmt:message key="publication.publisher" bundle="${bundle}"/></p>
                            <select name="publisher" size="1">
                                <c:forEach items="${requestScope.publishers}" var="publisher">
                                    <option value ="${publisher.id}">${publisher.company}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="field-block input-block">
                            <p><fmt:message key="publication.cost-per-month" bundle="${bundle}"/></p>
                            <input  required type="number" name="cost-per-month" step=".01" min="0.01" pattern="^[0-9]+((\.|,)[0-9]{1,2})?$">
                        </div>
                        <div class="field-block">
                            <p><fmt:message key="publication.publication-type" bundle="${bundle}"/></p>
                            <c:forEach items="${PublicationType.values()}" var="publicationType">
                                <label title="${publicationType.getValue()}">
                                    <input required type="radio" name="publication-type" value="${publicationType.value}">
                                    <fmt:message key="publication-type.${publicationType.value}" bundle="${bundle}"/>
                                </label>
                            </c:forEach>
                        </div>
                        <input type="submit" value="<fmt:message key="button.add" bundle="${bundle}"/>" class="add-button">
                    </form>
                </div>
                <div class="text-info">
                    <h3><fmt:message key="publisher.add-publisher" bundle="${bundle}"/></h3>
                </div>
                <div class="add-form-wrapper">
                    <form name="add-publisher-form" action="${pageContext.servletContext.contextPath}/controller?command=add_publisher" method="post">
                        <div class="field-block input-block">
                            <p><fmt:message key="publisher.company-name" bundle="${bundle}"/></p>
                            <input required type="text" name="company" pattern="^[A-Za-z0-9',. ]{1,40}$">
                        </div>
                        <input type="submit" value="<fmt:message key="button.add" bundle="${bundle}"/>" class="add-button">
                    </form>
                </div>
            </div>
        </section>

    </div>
</body>