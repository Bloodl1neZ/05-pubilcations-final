<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 12/23/2018
  Time: 3:38 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : (not empty language ? language : pageContext.request.locale)}" scope="session"/>

<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="pagecontent" var="bundle"/>
<%@ page import="com.epam.web.entity.Role" %>
<html>
<head>
    <title><fmt:message key="users.edit-users" bundle="${bundle}"/></title>
</head>
<body>
<jsp:include page="../fragments/header.jsp"></jsp:include>
<div class="all-wrapper">
    <section class="users-section">
        <table class="users-table">
            <thead>
            <tr>
                <th><fmt:message key="users.id" bundle="${bundle}"/></th>
                <th><fmt:message key="users.login" bundle="${bundle}"/></th>
                <th><fmt:message key="users.name" bundle="${bundle}"/></th>
                <th><fmt:message key="users.role" bundle="${bundle}"/></th>
                <th><fmt:message key="users.status" bundle="${bundle}"/></th>
                <th><fmt:message key="users.change-status" bundle="${bundle}"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${requestScope.users}" var="user">
            <tr>
                <td>${user.id}</td>
                <td>${user.login}</td>
                <td>${user.name} ${user.surname}</td>
                <td>${user.role.getValue()}</td>
                <td>
                    <c:if test="${user.active}">
                        <fmt:message key="users.active" bundle="${bundle}"/>
                    </c:if>
                    <c:if test="${!user.active}">
                        <fmt:message key="users.inactive" bundle="${bundle}"/>
                    </c:if>
                </td>
                <td>
                    <c:if test="${user.role != Role.ADMIN}">
                        <form action="${pageContext.servletContext.contextPath}/controller?command=change_active_status" method="post">
                            <input type="hidden" name="id" value="${user.id}" >
                            <input type="submit" value="<c:if test="${user.active}"><fmt:message key="users.lock" bundle="${bundle}"/></c:if><c:if test="${!user.active}"><fmt:message key="users.unlock" bundle="${bundle}"/></c:if>">
                        </form>
                    </c:if>
                </td>
            </tr>
            </c:forEach>

        </table>
    </section>
</div>
</body>
</html>
