<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 12/23/2018
  Time: 6:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="language" value="${not empty param.language ? param.language : (not empty language ? language : pageContext.request.locale)}" scope="session"/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="pagecontent" var="bundle"/>

<html>
<head>
    <title><fmt:message key="error.error" bundle="${bundle}"/></title>
</head>
<body>
    <jsp:include page="../fragments/header.jsp"></jsp:include>

    <div class="all-wrapper">
        <section class="error">
            <h2><fmt:message key="error.oops" bundle="${bundle}"/></h2>
            <c:if test="${requestScope.error != null}">
                <p><fmt:message key="${requestScope.error}" bundle="${bundle}"/></p>
            </c:if>
            <p>${pageContext.exception.message}</p>
        </section>
    </div>
</body>
</html>
