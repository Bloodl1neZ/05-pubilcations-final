<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 11/30/2018
  Time: 7:32 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="language" value="${not empty param.language ? param.language : (not empty language ? language : pageContext.request.locale)}" scope="session"/>

<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="pagecontent" var="bundle"/>

<html>
<head>
    <title><fmt:message key="users.log-in-page" bundle="${bundle}"/></title>
</head>
<body>
    <div class="all-wrapper">
        <jsp:include page="../fragments/header.jsp"/>
        <div class="log-in-window">
            <div class="text-info">
                <h3><fmt:message key="users.log-in-page" bundle="${bundle}"/></h3>
                <c:if test="${requestScope.error != null}">
                    <h3><fmt:message key="error.error" bundle="${bundle}"/>: <fmt:message key="${requestScope.error}" bundle="${bundle}"/></h3>
                </c:if>
            </div>
            <div class="form-wrapper">
                <form action="${pageContext.servletContext.contextPath}/controller?command=login" method="post">
                    <div class="field">
                        <input required class="text" type="text" name="login" placeholder="<fmt:message key="users.login" bundle="${bundle}"/>"/>
                    </div>
                    <div class="field">
                        <input required class="text" type="password" name="password" placeholder="<fmt:message key="users.password" bundle="${bundle}"/>"/>
                    </div>
                    <div class="button-wrapper">
                        <input class="button" type="submit" value="<fmt:message key="users.log-in" bundle="${bundle}"/>">
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
