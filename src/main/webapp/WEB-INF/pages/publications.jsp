<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 12/16/2018
  Time: 1:52 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="pg" uri="paginationTag" %>
<%@ page import="com.epam.web.entity.PublicationType" %>
<%@ page import="com.epam.web.entity.Role" %>
<%@ page import="com.epam.web.utils.MonthUtils" %>
<c:set var="language" value="${not empty param.language ? param.language : (not empty language ? language : pageContext.request.locale)}" scope="session"/>

<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="pagecontent" var="bundle"/>

<html>
<head>
    <title><fmt:message key="publication.publications" bundle="${bundle}"/></title>
</head>
<body>

    <div class="all-wrapper">
        <jsp:include page="../fragments/header.jsp"></jsp:include>
        <section class="publications-section">
            <div class="text-content">
                <h2><fmt:message key="publication.publications" bundle="${bundle}"/></h2>
            </div>
            <jsp:include page="../fragments/search.jsp"/>

            <div class="filter-publications-wrapper clearfix">
                <div class="filter-wrapper content-part">
                    <p><fmt:message key="publication-filter.filter-publications" bundle="${bundle}"/></p>
                    <form name="filter-form" action="${pageContext.servletContext.contextPath}/controller"
                          method="get">
                        <input type="hidden" name="command" value="show_publications_by_type_and_cost"/>
                        <div class="parameter-block">
                            <label>
                                <fmt:message key="publication.publication-type" bundle="${bundle}"/>
                                <c:forEach items="${PublicationType.values()}" var="publicationType">
                                    <label title="${publicationType.value}">
                                        <input required type="radio" name="publication-type" value="${publicationType.value}" class="radio-input">
                                        <fmt:message key="publication-type.${publicationType.value}" bundle="${bundle}"/>
                                    </label>
                                </c:forEach>
                            </label>
                        </div>
                        <div class="parameter-block">
                            <label>
                                <fmt:message key="publication-filter.min-cost" bundle="${bundle}"/>
                                <input type="number" name="min-cost" step=".01" min="0" class="min-cost" pattern="^[0-9]+((\.|,)[0-9]{1,2})?$">
                            </label>
                        </div>
                        <div class="parameter-block">
                            <label>
                                <fmt:message key="publication-filter.max-cost" bundle="${bundle}"/>
                                <input type="number" name="max-cost" step=".01" min="0.01" class="max-cost" pattern="^[0-9]+((\.|,)[0-9]{1,2})?$">
                            </label>
                        </div>
                        <div class="parameter-block">
                            <input type="submit" value="<fmt:message key="publication-filter.filter-button" bundle="${bundle}"/>" class="search-button">
                        </div>
                    </form>
                </div>

                <c:set var="page" value="${param.page == null ? 1 : param.page}"/>
                <c:set var="publicationsInPageAmount" value="9"/>
                <fmt:parseNumber var="pagePartAmount" value="${publications.size() div publicationsInPageAmount}" integerOnly="true"/>
                <c:set var="pageAmount" value="${pagePartAmount + ((publications.size() % publicationsInPageAmount > 0) ? 1 : 0)}"/>
                <c:choose>
                    <c:when test="${page <= 0}">
                        <c:set var="page" value="1"/>
                    </c:when>
                    <c:when test="${page > pageAmount}">
                        <c:set var="page" value="${pageAmount}"/>
                    </c:when>
                </c:choose>

                <div class="all-publications-wrapper content-part">
                    <div class="all-publications">
                        <c:choose>
                            <c:when test="${publications.isEmpty()}">
                                <p class="not-found-info">
                                    <fmt:message key="publication.not-found" bundle="${bundle}"/>
                                </p>
                            </c:when>
                            <c:otherwise>
                                <div class="publications-wrapper clearfix">
                                    <c:forEach items="${requestScope.publications}" var="publicationDto" begin="${(page - 1) * publicationsInPageAmount}" end="${page * publicationsInPageAmount - 1}" step="1">
                                        <c:set var="publication" value="${publicationDto.publication}"/>
                                        <c:set var="publisher" value="${publicationDto.publisher}"/>
                                        <div class="publication">
                                            <img src="../img/${publication.publicationType.value}.svg" alt="${publication.publicationType.value}">
                                            <div class="text-info">
                                                <p>${publication.title}</p>
                                                <p>${publisher.company}</p>
                                                <p>${publication.costPerMonth}$ <fmt:message key="publication.per-month" bundle="${bundle}"/></p>
                                            </div>

                                            <c:if test="${sessionScope.user != null && sessionScope.user.role == Role.ADMIN}">
                                                <button class="edit-button" id="open-modal-button" onclick="
                                                        setValuesInModal(${publication.id}, '${publication.title}', '${publication.publisherId}', ${publication.costPerMonth}, '${publication.publicationType.value}')">
                                                    <fmt:message key="publication.edit-button" bundle="${bundle}"/>
                                                </button>
                                            </c:if>
                                            <c:if test="${sessionScope.user != null && sessionScope.user.role == Role.USER}">
                                                <form class="add-to-order-form" action="${pageContext.servletContext.contextPath}/controller?command=add_subscription&publication-id=${publication.id}" method="post">
                                                    <input type="submit" name="add-to-cart-button" value="<fmt:message key="publication.add-subscription-to-order" bundle="${bundle}"/>">
                                                    <select name="month-amount" size="1">
                                                        <c:forEach items="${MonthUtils.AVAILABLE_MONTHS}" var="month">
                                                            <option value="${month}">
                                                                ${month} <fmt:message key="subscription.month-${month}" bundle="${bundle}"/>
                                                            </option>
                                                        </c:forEach>
                                                    </select>
                                                </form>
                                            </c:if>
                                        </div>
                                    </c:forEach>
                                </div>

                                <fmt:message key="page.next" bundle="${bundle}" var="next"/>
                                <fmt:message key="page.prev" bundle="${bundle}" var="prev"/>
                                <pg:pagination page="${page}" itemsAmount="${publications.size()}" amountOfItemsOnPage="${publicationsInPageAmount}" paginationWrapperClass="pagination-wrapper clearfix" prevButtonClass="prev" nextButtonClass="next" prevValue="${prev}" nextValue="${next}"/>

                            </c:otherwise>
                        </c:choose>

                    </div>

                </div>
            </div>

        </section>
        <c:if test="${sessionScope.user != null && sessionScope.user.role == Role.ADMIN}">

            <div id="edit-modal" class="modal">
                <!-- Modal content -->
                <div class="modal-content">
                    <span class="edit-close close">&times;</span>
                    <form name="edit-publication-form" action="${pageContext.servletContext.contextPath}/controller?command=edit_publication" method="post">
                        <div class="edit-input text-edit id-input input-block">
                            <input hidden type="number" name="id" pattern="^[0-9]+$">
                        </div>
                        <div class="edit-input text-edit input-block">
                            <p><fmt:message key="publication.title" bundle="${bundle}"/></p>
                            <input required type="text" name="title" pattern="^[A-Za-z0-9',. ]{1,40}$">
                        </div>
                        <div class="edit-input select-edit input-block">
                            <p><fmt:message key="publication.publisher" bundle="${bundle}"/></p>
                            <select name="publisher" size="1">
                                <c:forEach items="${requestScope.publishers}" var="publisher">
                                    <option value ="${publisher.id}">${publisher.company}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="edit-input text-edit input-block">
                            <p><fmt:message key="publication.cost-per-month" bundle="${bundle}"/></p>
                            <input required type="number" name="cost-per-month" step=".01" min="0.01" pattern="^[0-9]+((\.|,)[0-9]{1,2})?$">
                        </div>
                        <div class="edit-input input-block">
                            <p><fmt:message key="publication.publication-type" bundle="${bundle}"/></p>
                            <c:forEach items="${PublicationType.values()}" var="publicationType">
                                <label title="${publicationType.getValue()}">
                                    <input required type="radio" name="publication-type" value="${publicationType.value}">
                                    <fmt:message key="publication-type.${publicationType.value}" bundle="${bundle}"/>
                                </label>
                            </c:forEach>
                        </div>
                        <input type="submit" value="<fmt:message key="publication.save-button" bundle="${bundle}"/>" class="submit-changes-button">
                    </form>
                </div>
            </div>
        </c:if>
    </div>
</body>
<script src="${pageContext.request.contextPath}/js/publications-modal.js"></script>

</html>
