<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 12/25/2018
  Time: 2:48 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="UTF-8" isELIgnored="false" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />

<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="pagecontent" var="bundle"/>

<div class="search-by-title">
    <form action="${pageContext.servletContext.contextPath}/controller?command=show_publications_by_title" method="get">
        <div class="field">
            <input type="hidden" name="command" value="show_publications_by_title"/>
            <input required class="text" type="text" name="title" pattern="^[A-Za-z0-9',. ]{1,40}$" placeholder="<fmt:message key="main.search-by-title" bundle="${bundle}"/>"/>
            <div class="button-wrapper">
                <input class="button" type="submit" value="">
            </div>
        </div>
    </form>
</div>
