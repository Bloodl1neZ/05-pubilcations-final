<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page language="java" contentType="text/html; charset=utf-8"  pageEncoding="UTF-8" isELIgnored="false" %>
<%@ page import="com.epam.web.entity.Role" %>
<c:set var="language" value="${not empty param.language ? param.language : (not empty language ? language : pageContext.request.locale)}" scope="session"/>
<c:set var="publisherService" value="${PublisherService}"/>

<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="pagecontent" var="bundle"/>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/reset.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/styles.css" />
    <script src="${pageContext.request.contextPath}/js/header-modal.js"></script>
</head>
<header>
    <div class="button-wrapper clearfix">
        <div class="hamburger" id="hamburger-open" onclick="openModal()">
            <span class="x"></span>
            <span class="y"></span>
            <span class="z"></span>
        </div>

        <div class="left-align" id="header-left">
            <div class="hamburger h-close" id="hamburger-close" onclick="closeModal()">
                <span class="x"></span>
                <span class="y"></span>
            </div>
            <div class="header-element">
                <a href="${pageContext.servletContext.contextPath}/controller?command=go_home_page" class="home-button"><fmt:message key="header.home" bundle="${bundle}"/></a>
            </div>
            <c:if test="${sessionScope.user != null && sessionScope.user.getRole() == Role.ADMIN}">
                <div class="header-element">
                    <a href="${pageContext.servletContext.contextPath}/controller?command=go_users_page"><fmt:message key="header.edit-users" bundle="${bundle}"/></a>
                </div>
            </c:if>
            <c:if test="${sessionScope.user != null && sessionScope.user.getRole() == Role.ADMIN}">
                <div class="header-element">
                    <a href="${pageContext.servletContext.contextPath}/controller?command=go_publication_manager_page"><fmt:message key="header.add-publication" bundle="${bundle}"/></a>
                </div>
            </c:if>
        </div>
        <div class="right-align">
            <c:if test="${sessionScope.user != null}">
                <div class="header-element">
                    <p><fmt:message key="header.greetings" bundle="${bundle}"/> ${sessionScope.user.getName()}</p>
                </div>
            </c:if>
            <c:if test="${sessionScope.user == null}">
                <div class="header-element">
                    <a href="${pageContext.servletContext.contextPath}/controller?command=go_login_page" class="log-in-button"><fmt:message key="header.login" bundle="${bundle}"/></a>
                </div>
            </c:if>
            <c:if test="${sessionScope.user != null && sessionScope.user.role == Role.USER}">
                <div class="header-element">
                    <a href="${pageContext.servletContext.contextPath}/controller?command=go_order_page" class="order-button"><fmt:message key="header.order" bundle="${bundle}"/></a>
                </div>
            </c:if>

            <c:if test="${sessionScope.user != null}">
                <div class="header-element">
                    <form action="${pageContext.servletContext.contextPath}/controller?command=log_out" class="log-out-button" method="post">
                        <input type="submit"class="log-out-button" value="<fmt:message key="header.log-out" bundle="${bundle}"/>">
                    </form>
                </div>
            </c:if>
            <div class="header-element">
                <c:set var="parametersString" value="${pageContext.request.queryString}"/>
                <c:set var="lang" value="language"/>
                <c:if test="${fn:contains(parametersString, lang)}">
                    <c:set var="paramsStringsWithLangAmpersant" value="${fn:substringBefore(parametersString, lang)}"/>
                    <c:set var="parametersString" value="${fn:substring(paramsStringsWithLangAmpersant, 0, fn:length(paramsStringsWithLangAmpersant) - 1)}"/>
                </c:if>
                <c:choose>
                    <c:when test="${fn:length(parametersString)==0}">
                        <a href="${requestScope.requestURI}?language=<fmt:message key="header.lang" bundle="${bundle}"/>">
                            <fmt:message key="header.lang" bundle="${bundle}"/>
                        </a>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${language != 'ru_RU'}">
                            <a href="${requestScope.requestURI}?${parametersString}&language=ru_RU">
                                ru
                            </a>
                        </c:if>
                        <c:if test="${language != 'en_EN'}">
                            <a href="${requestScope.requestURI}?${parametersString}&language=en_EN">
                                en
                            </a>
                        </c:if>

                    </c:otherwise>
                </c:choose>

            </div>
        </div>
    </div>
</header>


