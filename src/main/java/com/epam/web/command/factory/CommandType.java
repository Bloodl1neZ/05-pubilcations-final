package com.epam.web.command.factory;

public enum CommandType {
    LOGIN("login"),
    LOG_OUT("log_out"),
    SHOW_ALL_PUBLICATIONS("show_all_publications"),
    SHOW_PUBLICATIONS_BY_TYPE("show_publications_by_type"),
    SHOW_PUBLICATIONS_BY_TITLE("show_publications_by_title"),
    SHOW_PUBLICATIONS_BY_TYPE_AND_COST("show_publications_by_type_and_cost"),
    CHANGE_ACTIVE_STATUS("change_active_status"),
    EDIT_PUBLICATION("edit_publication"),
    ADD_PUBLICATION("add_publication"),
    ADD_SUBSCRIPTION("add_subscription"),
    CHANGE_MONTH_AMOUNT_IN_SUBSCRIPTION("change_month_amount_in_subscription"),
    GO_HOME_PAGE("go_home_page"),
    GO_LOGIN_PAGE("go_login_page"),
    GO_ORDER_PAGE("go_order_page"),
    GO_USERS_PAGE("go_users_page"),
    DELETE_SUBSCRIPTION_BY_ID("delete_subscription_by_id"),
    PAY_FOR_ORDER("pay_for_order"),
    GO_ORDER_PAGE_BY_ORDER_ID("go_order_page_by_order_id"),
    GO_PUBLICATION_MANAGER_PAGE("go_publication_manager_page"),
    ADD_PUBLISHER("add_publisher");

    private final String value;

    CommandType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
