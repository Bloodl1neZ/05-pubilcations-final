package com.epam.web.command.factory;

import com.epam.web.command.Command;
import com.epam.web.command.admin.*;
import com.epam.web.command.common.*;
import com.epam.web.command.user.*;
import com.epam.web.utils.Page;

public class CommandFactory {

    public Command create(String command) {
        String commandUpper = command.toUpperCase();
        CommandType commandType = CommandType.valueOf(commandUpper);
        switch (commandType) {
            case LOGIN:
                return new LogInCommand();
            case LOG_OUT:
                return new LogOutCommand();
            case SHOW_ALL_PUBLICATIONS:
                return new ShowAllPublicationsCommand();
            case SHOW_PUBLICATIONS_BY_TYPE:
                return new ShowPublicationsByTypeCommand();
            case SHOW_PUBLICATIONS_BY_TITLE:
                return new ShowPublicationsByTitleCommand();
            case SHOW_PUBLICATIONS_BY_TYPE_AND_COST:
                return new ShowPublicationsByTypeAndCostCommand();
            case CHANGE_ACTIVE_STATUS:
                return new ChangeActiveStatusCommand();
            case EDIT_PUBLICATION:
                return new EditPublicationCommand();
            case ADD_PUBLICATION:
                return new AddPublicationCommand();
            case ADD_SUBSCRIPTION:
                return new AddSubscriptionCommand();
            case CHANGE_MONTH_AMOUNT_IN_SUBSCRIPTION:
                return new ChangeMonthAmountInSubscriptionCommand();
            case GO_HOME_PAGE:
                return new GoPageCommand(Page.HOME.getValue());
            case GO_LOGIN_PAGE:
                return new GoPageCommand(Page.LOGIN.getValue());
            case GO_ORDER_PAGE:
                return new GoOrderPageCommand();
            case GO_USERS_PAGE:
                return new GoUsersPageCommand();
            case GO_PUBLICATION_MANAGER_PAGE:
                return new GoPublicationManagerPageCommand();
            case GO_ORDER_PAGE_BY_ORDER_ID:
                return new GoOrderPageByOrderIdCommand();
            case DELETE_SUBSCRIPTION_BY_ID:
                return new DeleteSubscriptionCommand();
            case PAY_FOR_ORDER:
                return new PayForOrderCommand();
            case ADD_PUBLISHER:
                return new AddPublisherCommand();
            default:
                throw new IllegalArgumentException("unsupported command");
        }
    }
}
