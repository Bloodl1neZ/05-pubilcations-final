package com.epam.web.command.common;

import com.epam.web.command.Command;
import com.epam.web.command.CommandResult;
import com.epam.web.entity.Publisher;
import com.epam.web.entity.dto.PublicationDto;
import com.epam.web.exception.CommandException;
import com.epam.web.exception.ServiceException;
import com.epam.web.service.PublicationDtoService;
import com.epam.web.service.PublisherService;
import com.epam.web.utils.Page;
import com.epam.web.utils.RegExHelper;
import com.epam.web.utils.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ShowPublicationsByTypeAndCostCommand implements Command {
    private static final String PUBLICATION_TYPE_PARAMETER = "publication-type";
    private static final String MIN_COST_PARAMETER = "min-cost";
    private static final String MAX_COST_PARAMETER = "max-cost";

    private static final String PUBLICATIONS_ATTRIBUTE = "publications";
    private static final String PUBLISHERS_ATTRIBUTE = "publishers";
    private static final String ERROR_ATTRIBUTE = "error";

    private static final String ERROR_MESSAGE = "error.reg-ex-message";


    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            String publicationType = request.getParameter(PUBLICATION_TYPE_PARAMETER);
            String minCost = request.getParameter(MIN_COST_PARAMETER);
            String maxCost = request.getParameter(MAX_COST_PARAMETER);

            CommandResult commandResult;

            if (StringUtils.isMatchesToRegEx(minCost, RegExHelper.MONEY_REGEX, true)
                    && StringUtils.isMatchesToRegEx(maxCost, RegExHelper.MONEY_REGEX, true)) {

                PublicationDtoService service = new PublicationDtoService();
                List<PublicationDto> publications = service.getPublicationsByTypeAndCost(publicationType, minCost, maxCost);
                request.setAttribute(PUBLICATIONS_ATTRIBUTE, publications);

                PublisherService publisherService = new PublisherService();
                List<Publisher> publishers = publisherService.getAllPublishers();
                request.setAttribute(PUBLISHERS_ATTRIBUTE, publishers);

                commandResult = CommandResult.forward(Page.PUBLICATIONS.getValue());
            } else {
                request.setAttribute(ERROR_ATTRIBUTE, ERROR_MESSAGE);
                commandResult = CommandResult.forward(Page.ERROR.getValue());
            }
            return commandResult;

        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }
}
