package com.epam.web.command.common;

import com.epam.web.command.Command;
import com.epam.web.command.CommandResult;
import com.epam.web.entity.Publisher;
import com.epam.web.entity.dto.PublicationDto;
import com.epam.web.exception.CommandException;
import com.epam.web.exception.ServiceException;
import com.epam.web.service.PublicationDtoService;
import com.epam.web.service.PublisherService;
import com.epam.web.utils.Page;
import com.epam.web.utils.RegExHelper;
import com.epam.web.utils.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ShowPublicationsByTitleCommand implements Command {
    private static final String TITLE_PARAMETER = "title";

    private static final String PUBLICATIONS_ATTRIBUTE = "publications";
    private static final String PUBLISHERS_ATTRIBUTE = "publishers";

    private static final String ERROR_ATTRIBUTE = "error";
    private static final String ERROR_MESSAGE = "error.reg-ex-message";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            PublicationDtoService service = new PublicationDtoService();
            String title = request.getParameter(TITLE_PARAMETER);

            CommandResult commandResult;
            if (StringUtils.isMatchesToRegEx(title, RegExHelper.TITLE_REGEX, true)) {
                List<PublicationDto> publications = service.getPublicationsByTitle(title);
                request.setAttribute(PUBLICATIONS_ATTRIBUTE, publications);

                PublisherService publisherService = new PublisherService();
                List<Publisher> publishers = publisherService.getAllPublishers();
                request.setAttribute(PUBLISHERS_ATTRIBUTE, publishers);

                commandResult = CommandResult.forward(Page.PUBLICATIONS.getValue());
            } else {
                request.setAttribute(ERROR_ATTRIBUTE, ERROR_MESSAGE);
                commandResult = CommandResult.forward(Page.ERROR.getValue());
            }
            return commandResult;
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }
}
