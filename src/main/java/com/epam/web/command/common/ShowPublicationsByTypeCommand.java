package com.epam.web.command.common;

import com.epam.web.command.Command;
import com.epam.web.command.CommandResult;
import com.epam.web.entity.Publisher;
import com.epam.web.entity.dto.PublicationDto;
import com.epam.web.exception.CommandException;
import com.epam.web.exception.ServiceException;
import com.epam.web.service.PublicationDtoService;
import com.epam.web.service.PublisherService;
import com.epam.web.utils.Page;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ShowPublicationsByTypeCommand implements Command {
    private static final String PUBLICATION_TYPE_PARAMETER = "publication-type";

    private static final String PUBLICATIONS_ATTRIBUTE = "publications";
    private static final String PUBLISHERS_ATTRIBUTE = "publishers";


    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            PublicationDtoService service = new PublicationDtoService();
            String publicationType = request.getParameter(PUBLICATION_TYPE_PARAMETER);
            List<PublicationDto> publications = service.getPublicationsByType(publicationType);
            request.setAttribute(PUBLICATIONS_ATTRIBUTE, publications);

            PublisherService publisherService = new PublisherService();
            List<Publisher> publishers = publisherService.getAllPublishers();
            request.setAttribute(PUBLISHERS_ATTRIBUTE, publishers);

            return CommandResult.forward(Page.PUBLICATIONS.getValue());
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }
}
