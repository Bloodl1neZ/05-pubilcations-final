package com.epam.web.command.common;

import com.epam.web.command.Command;
import com.epam.web.command.CommandResult;
import com.epam.web.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GoPageCommand implements Command {
    private final String page;

    public GoPageCommand(String page) {
        this.page = page;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        return CommandResult.forward(page);
    }
}
