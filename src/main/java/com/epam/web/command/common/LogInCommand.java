package com.epam.web.command.common;

import com.epam.web.command.Command;
import com.epam.web.command.CommandResult;
import com.epam.web.command.attribute.SessionAttribute;
import com.epam.web.entity.User;
import com.epam.web.exception.CommandException;
import com.epam.web.exception.ServiceException;
import com.epam.web.service.UserService;
import com.epam.web.utils.Page;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Optional;

public class LogInCommand implements Command {
    private static final String LOGIN_PARAMETER = "login";
    private static final String PASSWORD_PARAMETER = "password";

    private static final String USER_NOT_FOUND_MESSAGE = "error.not-found";
    private static final String INACTIVE_USER_MESSAGE = "error.inactive-acc";

    private static final String ERROR_ATTRIBUTE = "error";

    private static final String HOME_PAGE = "/controller?command=go_home_page";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            HttpSession session = request.getSession(true);
            String login = request.getParameter(LOGIN_PARAMETER);
            String password = request.getParameter(PASSWORD_PARAMETER);
            UserService userService = new UserService();
            Optional<User> userOptional = userService.logIn(login, password);
            CommandResult commandResult;
            if (userOptional.isPresent()) {
                User user = userOptional.get();
                if (user.isActive()) {
                    session.setAttribute(SessionAttribute.USER_ATTRIBUTE, user);
                    commandResult = CommandResult.redirect(HOME_PAGE);
                } else {
                    request.setAttribute(ERROR_ATTRIBUTE, INACTIVE_USER_MESSAGE);
                    commandResult = CommandResult.forward(Page.LOGIN.getValue());
                }
            } else {
                request.setAttribute(ERROR_ATTRIBUTE, USER_NOT_FOUND_MESSAGE);
                commandResult = CommandResult.forward(Page.LOGIN.getValue());
            }
            return commandResult;
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }
}
