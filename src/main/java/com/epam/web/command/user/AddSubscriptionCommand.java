package com.epam.web.command.user;

import com.epam.web.command.Command;
import com.epam.web.command.CommandResult;
import com.epam.web.command.attribute.SessionAttribute;
import com.epam.web.entity.User;
import com.epam.web.exception.CommandException;
import com.epam.web.exception.ServiceException;
import com.epam.web.service.SubscriptionService;
import com.epam.web.utils.RegExHelper;
import com.epam.web.utils.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AddSubscriptionCommand implements Command {
    private static final String PUBLICATION_ID_PARAMETER = "publication-id";
    private static final String MONTH_AMOUNT_PARAMETER = "month-amount";

    private static final String ORDER_PAGE = "/controller?command=go_order_page";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            String publicationId = request.getParameter(PUBLICATION_ID_PARAMETER);
            String monthAmount = request.getParameter(MONTH_AMOUNT_PARAMETER);

            if (areAllMatchesToRegEx(publicationId, monthAmount)) {
                HttpSession session = request.getSession();

                User user = (User)session.getAttribute(SessionAttribute.USER_ATTRIBUTE);
                int userId = user.getId();

                SubscriptionService subscriptionService = new SubscriptionService();
                subscriptionService.addSubscriptionByUserIdAndPublicationIdAndMonthAmount(userId, publicationId, monthAmount);
            }

            return CommandResult.redirect(ORDER_PAGE);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }

    private boolean areAllMatchesToRegEx(String publicationId, String monthAmount) {
        return StringUtils.isMatchesToRegEx(publicationId, RegExHelper.ID_REGEX, false)
                && StringUtils.isMatchesToRegEx(monthAmount, RegExHelper.ID_REGEX, false);
    }


}
