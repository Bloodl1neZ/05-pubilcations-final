package com.epam.web.command.user;

import com.epam.web.command.Command;
import com.epam.web.command.CommandResult;
import com.epam.web.command.attribute.SessionAttribute;
import com.epam.web.entity.User;
import com.epam.web.exception.CommandException;
import com.epam.web.exception.ServiceException;
import com.epam.web.service.OrderService;
import com.epam.web.utils.RegExHelper;
import com.epam.web.utils.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class PayForOrderCommand implements Command {
    private static final String ORDER_PAGE = "/controller?command=go_order_page";
    private static final String NAME_ATTRIBUTE = "name";
    private static final String NUMBER_ATTRIBUTE = "number";
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            String name = request.getParameter(NAME_ATTRIBUTE);
            String number = request.getParameter(NUMBER_ATTRIBUTE);

            if (areAllMatchesToRegEx(name, number)) {
                HttpSession httpSession = request.getSession();
                User user = (User)httpSession.getAttribute(SessionAttribute.USER_ATTRIBUTE);
                int userId = user.getId();

                OrderService orderService = new OrderService();
                orderService.payForOrder(userId);
            }
            return CommandResult.redirect(ORDER_PAGE);


        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }

    private boolean areAllMatchesToRegEx(String name, String number) {
        return StringUtils.isMatchesToRegEx(name, RegExHelper.NAME_REGEX, false)
                && StringUtils.isMatchesToRegEx(number, RegExHelper.CREDIT_CARD_REGEX, false);
    }
}
