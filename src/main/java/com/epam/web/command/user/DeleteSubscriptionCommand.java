package com.epam.web.command.user;

import com.epam.web.command.Command;
import com.epam.web.command.CommandResult;
import com.epam.web.entity.User;
import com.epam.web.exception.CommandException;
import com.epam.web.exception.ServiceException;
import com.epam.web.service.SubscriptionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class DeleteSubscriptionCommand implements Command {
    private static final String ORDER_PAGE = "/controller?command=go_order_page";
    private static final String USER_ATTRIBUTE = "user";
    private static final String ID_PARAMETER = "id";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            String id = request.getParameter(ID_PARAMETER);
            SubscriptionService subscriptionService = new SubscriptionService();

            HttpSession session = request.getSession();
            User user = (User) session.getAttribute(USER_ATTRIBUTE);
            int userId = user.getId();
            subscriptionService.deleteSubscriptionByIdAndUserId(id, userId);

            return CommandResult.redirect(ORDER_PAGE);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }
}
