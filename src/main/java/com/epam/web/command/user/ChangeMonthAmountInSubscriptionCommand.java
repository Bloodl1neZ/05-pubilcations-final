package com.epam.web.command.user;

import com.epam.web.command.Command;
import com.epam.web.command.CommandResult;
import com.epam.web.entity.User;
import com.epam.web.exception.CommandException;
import com.epam.web.exception.ServiceException;
import com.epam.web.service.SubscriptionService;
import com.epam.web.utils.RegExHelper;
import com.epam.web.utils.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ChangeMonthAmountInSubscriptionCommand implements Command {
    private static final String MONTH_AMOUNT_PARAMETER = "month-amount";
    private static final String SUBSCRIPTION_ID_PARAMETER = "subscription-id";

    private static final String USER_ATTRIBUTE = "user";

    private static final String ORDER_PAGE = "/controller?command=go_order_page";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            String subscriptionId = request.getParameter(SUBSCRIPTION_ID_PARAMETER);
            String monthAmount = request.getParameter(MONTH_AMOUNT_PARAMETER);

            if (areAllMatchesToRegEx(subscriptionId, monthAmount)) {
                HttpSession httpSession = request.getSession();
                User user = (User) httpSession.getAttribute(USER_ATTRIBUTE);
                int userId = user.getId();

                SubscriptionService subscriptionService = new SubscriptionService();
                subscriptionService.changeMonthAmountOfSubscriptionBySubscriptionIdAndUserId(subscriptionId, monthAmount, userId);
            }


            return CommandResult.redirect(ORDER_PAGE);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }

    private boolean areAllMatchesToRegEx(String subscriptionId, String monthAmount) {
        return StringUtils.isMatchesToRegEx(subscriptionId, RegExHelper.ID_REGEX, false)
                && StringUtils.isMatchesToRegEx(monthAmount, RegExHelper.ID_REGEX, false);
    }
}
