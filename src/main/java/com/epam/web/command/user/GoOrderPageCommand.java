package com.epam.web.command.user;

import com.epam.web.command.Command;
import com.epam.web.command.CommandResult;
import com.epam.web.command.attribute.SessionAttribute;
import com.epam.web.entity.Order;
import com.epam.web.entity.User;
import com.epam.web.entity.dto.OrderDto;
import com.epam.web.exception.CommandException;
import com.epam.web.exception.ServiceException;
import com.epam.web.service.OrderDtoService;
import com.epam.web.service.OrderService;
import com.epam.web.utils.Page;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class GoOrderPageCommand implements Command {
    private static final String ORDER_DTO = "orderDto";
    private static final String PAID_ORDERS = "paidOrders";
    private static final String CURRENT_ORDER = "currentOrder";

    private static final String ORDER_PAGE = Page.ORDER.getValue();

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            HttpSession httpSession = request.getSession();
            User user = (User)httpSession.getAttribute(SessionAttribute.USER_ATTRIBUTE);
            int userId = user.getId();

            OrderDtoService orderDtoService = new OrderDtoService();
            OrderDto orderDto = orderDtoService.getCurrentOrderByUserId(userId);

            request.setAttribute(ORDER_DTO, orderDto);

            Order currentOrder = orderDto.getOrder();
            request.setAttribute(CURRENT_ORDER, currentOrder);

            OrderService orderService = new OrderService();
            List<Order> paidOrders = orderService.getPaidOrdersByUserId(userId);

            request.setAttribute(PAID_ORDERS, paidOrders);

            return CommandResult.forward(ORDER_PAGE);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }
}
