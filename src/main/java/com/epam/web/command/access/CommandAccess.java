package com.epam.web.command.access;

import com.epam.web.command.factory.CommandType;
import com.epam.web.entity.Role;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CommandAccess {
    public List<CommandType> getAvailableCommandTypesByRole(Optional<Role> roleOptional) {
        List<CommandType> availableCommandTypes = new ArrayList<>(getCommonCommandTypes());
        if (roleOptional.isPresent()) {
            Role role = roleOptional.get();
            List<CommandType> commandTypesByRole = getCommandTypesByRole(role);
            availableCommandTypes.addAll(commandTypesByRole);
        } else {
            availableCommandTypes.addAll(getCommandTypesForNotAuthorized());
        }
        return availableCommandTypes;
    }

    private List<CommandType> getCommonCommandTypes() {
        List<CommandType> commandTypes = new ArrayList<>();
        commandTypes.add(CommandType.GO_HOME_PAGE);
        commandTypes.add(CommandType.SHOW_ALL_PUBLICATIONS);
        commandTypes.add(CommandType.SHOW_PUBLICATIONS_BY_TITLE);
        commandTypes.add(CommandType.SHOW_PUBLICATIONS_BY_TYPE);
        commandTypes.add(CommandType.SHOW_PUBLICATIONS_BY_TYPE_AND_COST);
        return commandTypes;
    }

    private List<CommandType> getCommandTypesByRole(Role role) {
        List<CommandType> commandTypes;
        switch (role) {
            case USER:
                commandTypes = getUserCommandTypes();
                break;
            case ADMIN:
                commandTypes = getAdminCommandTypes();
                break;
            default:
                throw new IllegalArgumentException("unsupported role");
        }
        return commandTypes;
    }


    private List<CommandType> getAdminCommandTypes() {
        List<CommandType> commandTypes = new ArrayList<>();
        commandTypes.add(CommandType.GO_USERS_PAGE);
        commandTypes.add(CommandType.ADD_PUBLICATION);
        commandTypes.add(CommandType.EDIT_PUBLICATION);
        commandTypes.add(CommandType.CHANGE_ACTIVE_STATUS);
        commandTypes.add(CommandType.LOG_OUT);
        commandTypes.add(CommandType.GO_PUBLICATION_MANAGER_PAGE);
        commandTypes.add(CommandType.ADD_PUBLISHER);
        return commandTypes;
    }

    private List<CommandType> getUserCommandTypes() {
        List<CommandType> commandTypes = new ArrayList<>();
        commandTypes.add(CommandType.GO_ORDER_PAGE);
        commandTypes.add(CommandType.ADD_SUBSCRIPTION);
        commandTypes.add(CommandType.CHANGE_MONTH_AMOUNT_IN_SUBSCRIPTION);
        commandTypes.add(CommandType.LOG_OUT);
        commandTypes.add(CommandType.DELETE_SUBSCRIPTION_BY_ID);
        commandTypes.add(CommandType.PAY_FOR_ORDER);
        commandTypes.add(CommandType.GO_ORDER_PAGE_BY_ORDER_ID);
        return commandTypes;
    }

    private List<CommandType> getCommandTypesForNotAuthorized() {
        List<CommandType> commandTypes = new ArrayList<>();
        commandTypes.add(CommandType.LOGIN);
        commandTypes.add(CommandType.GO_LOGIN_PAGE);
        return commandTypes;
    }
}
