package com.epam.web.command.admin;

import com.epam.web.command.Command;
import com.epam.web.command.CommandResult;
import com.epam.web.entity.Publication;
import com.epam.web.entity.Publisher;
import com.epam.web.exception.CommandException;
import com.epam.web.exception.ServiceException;
import com.epam.web.service.PublicationService;
import com.epam.web.service.PublisherService;
import com.epam.web.utils.RegExHelper;
import com.epam.web.utils.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class AddPublicationCommand implements Command {
    private static final String TITLE_PARAMETER = "title";
    private static final String PUBLISHER_PARAMETER = "publisher";
    private static final String PUBLICATION_TYPE_PARAMETER = "publication-type";
    private static final String COST_PER_MONTH_PARAMETER = "cost-per-month";

    private static final String PUBLICATIONS_PAGE = "/controller?command=show_all_publications";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            String title = request.getParameter(TITLE_PARAMETER);
            String publisherId = request.getParameter(PUBLISHER_PARAMETER);
            String publicationType = request.getParameter(PUBLICATION_TYPE_PARAMETER);
            String costPerMonth = request.getParameter(COST_PER_MONTH_PARAMETER);

            if (areAllMatchesToRegEx(title, publisherId, costPerMonth)) {
                PublicationService publicationService = new PublicationService();
                publicationService.addPublication(title, publisherId, costPerMonth, publicationType);
            }

            return CommandResult.redirect(PUBLICATIONS_PAGE);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }

    }

    private boolean areAllMatchesToRegEx(String title, String publisherId, String costPerMonth) {
        return StringUtils.isMatchesToRegEx(title, RegExHelper.TITLE_REGEX, false)
                && StringUtils.isMatchesToRegEx(publisherId, RegExHelper.ID_REGEX, false)
                && StringUtils.isMatchesToRegEx(costPerMonth, RegExHelper.MONEY_REGEX, false);
    }

}
