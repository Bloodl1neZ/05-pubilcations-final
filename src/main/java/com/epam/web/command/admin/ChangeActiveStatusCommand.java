package com.epam.web.command.admin;

import com.epam.web.command.Command;
import com.epam.web.command.CommandResult;
import com.epam.web.exception.CommandException;
import com.epam.web.exception.ServiceException;
import com.epam.web.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ChangeActiveStatusCommand implements Command {
    private static final String ID_PARAMETER = "id";

    private static final String USERS_PAGE = "/controller?command=go_users_page";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            String id = request.getParameter(ID_PARAMETER);
            UserService userService = new UserService();
            userService.changeActiveStatus(id);

            return CommandResult.redirect(USERS_PAGE);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }

    }
}
