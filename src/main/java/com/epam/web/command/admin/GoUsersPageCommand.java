package com.epam.web.command.admin;

import com.epam.web.command.Command;
import com.epam.web.command.CommandResult;
import com.epam.web.exception.CommandException;
import com.epam.web.exception.ServiceException;
import com.epam.web.service.UserService;
import com.epam.web.utils.Page;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GoUsersPageCommand implements Command {
    private static final String USERS_PAGE = Page.USERS.getValue();
    private static final String USERS_ATTRIBUTE = "users";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            UserService userService = new UserService();
            request.setAttribute(USERS_ATTRIBUTE, userService.getAllUsers());
            return CommandResult.forward(USERS_PAGE);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }
}
