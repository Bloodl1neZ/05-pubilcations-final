package com.epam.web.command.admin;

import com.epam.web.command.Command;
import com.epam.web.command.CommandResult;
import com.epam.web.entity.Publisher;
import com.epam.web.exception.CommandException;
import com.epam.web.exception.ServiceException;
import com.epam.web.service.PublisherService;
import com.epam.web.utils.Page;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class GoPublicationManagerPageCommand implements Command {
    private static final String PUBLICATION_MANAGER_PAGE = Page.PUBLICATION_MANAGER.getValue();
    private static final String PUBLISHERS_ATTRIBUTE = "publishers";
    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            PublisherService publisherService = new PublisherService();
            List<Publisher> publishers = publisherService.getAllPublishers();

            request.setAttribute(PUBLISHERS_ATTRIBUTE, publishers);
            return CommandResult.forward(PUBLICATION_MANAGER_PAGE);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }
    }
}
