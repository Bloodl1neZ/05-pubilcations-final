package com.epam.web.command.admin;

import com.epam.web.command.Command;
import com.epam.web.command.CommandResult;
import com.epam.web.exception.CommandException;
import com.epam.web.exception.ServiceException;
import com.epam.web.service.PublisherService;
import com.epam.web.utils.RegExHelper;
import com.epam.web.utils.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddPublisherCommand implements Command {
    private static final String COMPANY_PARAMETER = "company";

    private static final String PUBLICATIONS_MANAGER_PAGE = "/controller?command=go_publication_manager_page";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            String company = request.getParameter(COMPANY_PARAMETER);
            if (StringUtils.isMatchesToRegEx(company, RegExHelper.TITLE_REGEX, false)) {
                PublisherService publisherService = new PublisherService();
                publisherService.addPublisher(company);
            }
            return CommandResult.redirect(PUBLICATIONS_MANAGER_PAGE);
        } catch (ServiceException e) {
            throw new CommandException(e.getMessage(), e);
        }

    }
}
