package com.epam.web.filter;

import com.epam.web.command.access.CommandAccess;
import com.epam.web.command.attribute.SessionAttribute;
import com.epam.web.command.factory.CommandType;
import com.epam.web.entity.Role;
import com.epam.web.entity.User;
import com.epam.web.utils.Page;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class RoleFilter implements Filter {
    private static final String COMMAND = "command";
    private static final String HOME_PAGE = Page.HOME.getValue();

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest)servletRequest;
        Optional<User> userOptional = getUserByRequest(httpServletRequest);

        Optional<Role> roleOptional;
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            Role role = user.getRole();
            roleOptional = Optional.of(role);
        } else {
            roleOptional = Optional.empty();
        }

        CommandAccess commandAccess = new CommandAccess();
        List<CommandType> commandTypes = commandAccess.getAvailableCommandTypesByRole(roleOptional);

        String command = servletRequest.getParameter(COMMAND);
        String commandUpper = command.toUpperCase();

        CommandType commandType = CommandType.valueOf(commandUpper);

        if (commandTypes.contains(commandType)) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            RequestDispatcher requestDispatcher = servletRequest.getRequestDispatcher(HOME_PAGE);
            requestDispatcher.forward(servletRequest, servletResponse);
        }
    }

    private Optional<User> getUserByRequest(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        Optional<User> userOptional;
        if (session == null) {
            userOptional = Optional.empty();
        } else {
            User user = (User)session.getAttribute(SessionAttribute.USER_ATTRIBUTE);
            userOptional = Optional.ofNullable(user);
        }
        return userOptional;
    }

    @Override
    public void destroy() {
    }
}
