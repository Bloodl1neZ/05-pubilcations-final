package com.epam.web.service;

import com.epam.web.entity.dto.SubscriptionDto;
import com.epam.web.exception.RepositoryException;
import com.epam.web.exception.ServiceException;
import com.epam.web.repository.Repository;
import com.epam.web.repository.factory.RepositoryFactory;
import com.epam.web.repository.specification.Specification;
import com.epam.web.repository.specification.dto.subscription.SubscriptionDtoByOrderIdSpecification;

import java.util.List;

public class SubscriptionDtoService {
    public List<SubscriptionDto> getSubscriptionDtosByOrderId(int orderId) throws ServiceException {
        return query(new SubscriptionDtoByOrderIdSpecification(orderId));
    }

    private List<SubscriptionDto> query(Specification specification) throws ServiceException {
        try (RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<SubscriptionDto> repository = repositoryFactory.getSubscriptionDtoRepository();
            return repository.query(specification);
        } catch (RepositoryException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
