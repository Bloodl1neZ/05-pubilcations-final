package com.epam.web.service;

import com.epam.web.entity.Role;
import com.epam.web.entity.User;
import com.epam.web.exception.RepositoryException;
import com.epam.web.exception.ServiceException;
import com.epam.web.repository.Repository;
import com.epam.web.repository.factory.RepositoryFactory;
import com.epam.web.repository.specification.Specification;
import com.epam.web.repository.specification.user.AllUsersSpecification;
import com.epam.web.repository.specification.user.UserByIdSpecification;
import com.epam.web.repository.specification.user.UserLoginPasswordSpecification;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.List;
import java.util.Optional;

public class UserService {
    public Optional<User> logIn(String login, String password) throws ServiceException {
        String md5Password = DigestUtils.md5Hex(password);
        return queryForSingleResult(new UserLoginPasswordSpecification(login, md5Password));
    }

    public List<User> getAllUsers() throws ServiceException {
        return query(new AllUsersSpecification());
    }

    public void changeActiveStatus(String id) throws ServiceException {
        int idValue = Integer.parseInt(id);
        Optional<User> userOptional = queryForSingleResult(new UserByIdSpecification(idValue));
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            Role role = user.getRole();
            if (role != Role.ADMIN) {
                String login = user.getLogin();
                String password = user.getPassword();
                String name = user.getName();
                String surname = user.getSurname();
                if (user.isActive()) {
                    save(new User(idValue, login, password, name, surname, role, false));
                } else {
                    save(new User(idValue, login, password, name, surname, role, true));
                }
            }
        }
    }

    private Optional<User> queryForSingleResult(Specification specification) throws ServiceException {
        try (RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<User> repository = repositoryFactory.getUserRepository();
            return repository.queryForSingleResult(specification);
        } catch (RepositoryException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    private List<User> query(Specification specification) throws ServiceException {
        try (RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<User> repository = repositoryFactory.getUserRepository();
            return repository.query(specification);
        } catch (RepositoryException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    private void save(User user) throws ServiceException {
        try (RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<User> repository = repositoryFactory.getUserRepository();
            repository.save(user);
        } catch (RepositoryException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
