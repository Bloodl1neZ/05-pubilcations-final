package com.epam.web.service;

import com.epam.web.entity.Publication;
import com.epam.web.entity.PublicationType;
import com.epam.web.entity.Publisher;
import com.epam.web.exception.RepositoryException;
import com.epam.web.exception.ServiceException;
import com.epam.web.repository.Repository;
import com.epam.web.repository.factory.RepositoryFactory;
import com.epam.web.repository.specification.Specification;
import com.epam.web.repository.specification.publication.PublicationByIdSpecification;

import java.math.BigDecimal;
import java.util.Optional;

public class PublicationService {

    public void editPublication(String id, String title, String publisherId, String costPerMonth, String publicationType) throws ServiceException {
        if (isCorrectPublisherId(publisherId)){
            Integer idValue = Integer.parseInt(id);
            BigDecimal costPerMonthValue = new BigDecimal(costPerMonth);
            String publicationTypeUpper = publicationType.toUpperCase();
            PublicationType publicationTypeValue = PublicationType.valueOf(publicationTypeUpper);
            int publisherIdValue = Integer.parseInt(publisherId);
            Publication publication = new Publication(idValue, title, publisherIdValue, costPerMonthValue, publicationTypeValue);
            save(publication);
        }
    }

    public void addPublication(String title, String publisherId, String costPerMonth, String publicationType) throws ServiceException {
        if (isCorrectPublisherId(publisherId)) {
            BigDecimal costPerMonthValue = new BigDecimal(costPerMonth);

            String publicationTypeUpper = publicationType.toUpperCase();
            PublicationType publicationTypeValue = PublicationType.valueOf(publicationTypeUpper);

            int publisherIdValue = Integer.parseInt(publisherId);
            Publication publication = new Publication(title, publisherIdValue, costPerMonthValue, publicationTypeValue);
            save(publication);
        }
    }

    public Optional<Publication> getPublicationById(String publicationId) throws ServiceException {
        int publicationIdValue = Integer.parseInt(publicationId);
        return queryForSingleResult(new PublicationByIdSpecification(publicationIdValue));
    }

    private boolean isCorrectPublisherId(String publisherId) throws ServiceException {
        PublisherService publisherService = new PublisherService();
        Optional<Publisher> publisherOptional = publisherService.getPublisherById(publisherId);
        return publisherOptional.isPresent();
    }

    private void save(Publication publication) throws ServiceException {
        try (RepositoryFactory factory = new RepositoryFactory()) {
            Repository<Publication> repository = factory.getPublicationRepository();
            repository.save(publication);
        } catch (RepositoryException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    private Optional<Publication> queryForSingleResult(Specification specification) throws ServiceException {
        try (RepositoryFactory factory = new RepositoryFactory()) {
            Repository<Publication> repository = factory.getPublicationRepository();
            return  repository.queryForSingleResult(specification);
        } catch (RepositoryException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
