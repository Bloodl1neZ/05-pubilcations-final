package com.epam.web.service;

import com.epam.web.entity.Order;
import com.epam.web.entity.dto.OrderDto;
import com.epam.web.entity.dto.SubscriptionDto;
import com.epam.web.exception.RepositoryException;
import com.epam.web.exception.ServiceException;
import com.epam.web.repository.Repository;
import com.epam.web.repository.factory.RepositoryFactory;
import com.epam.web.repository.specification.Specification;
import com.epam.web.repository.specification.order.OrderByIdSpecification;
import com.epam.web.repository.specification.order.OrdersByUserIdAndPaidSpecification;

import java.util.List;
import java.util.Optional;

public class OrderService {
    public Optional<Order> getOrderByOrderId(String orderId) throws ServiceException {
        int orderIdValue = Integer.parseInt(orderId);
        Specification specification = new OrderByIdSpecification(orderIdValue);
        return queryForSingleResult(specification);
    }

    public List<Order> getPaidOrdersByUserId(int usedId) throws ServiceException {
        Specification specification = new OrdersByUserIdAndPaidSpecification(usedId, true);
        return query(specification);
    }

    public void payForOrder(int userId) throws ServiceException {
        OrderDtoService orderDtoService = new OrderDtoService();
        OrderDto orderDto = orderDtoService.getCurrentOrderByUserId(userId);
        List<SubscriptionDto> subscriptions = orderDto.getSubscriptionDtos();

        if (!subscriptions.isEmpty()) {
            Order currentOrder = orderDto.getOrder();
            Integer orderId = currentOrder.getId();
            Order paidOrder = new Order(orderId, userId, true);
            save(paidOrder);
        }
    }

    public Order getCurrentOrderByUserId(int userId) throws ServiceException {
        Specification currentOrderByUserIdSpecification = new OrdersByUserIdAndPaidSpecification(userId, false);
        Optional<Order> orderOptional = queryForSingleResult(currentOrderByUserIdSpecification);
        Order currentOrder;
        if (orderOptional.isPresent()) {
            currentOrder = orderOptional.get();
        } else {
            save(new Order(userId, false));
            Optional<Order> createdOrderOptional = queryForSingleResult(currentOrderByUserIdSpecification);
            if (createdOrderOptional.isPresent()) {
                currentOrder = createdOrderOptional.get();
            } else {
                throw new IllegalStateException("it should be already created, it must exist");
            }

        }
        return currentOrder;
    }

    private Optional<Order> queryForSingleResult(Specification specification) throws ServiceException {
        try (RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<Order> repository = repositoryFactory.getOrderRepository();
            return repository.queryForSingleResult(specification);
        } catch (RepositoryException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    private List<Order> query(Specification specification) throws ServiceException {
        try (RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<Order> repository = repositoryFactory.getOrderRepository();
            return repository.query(specification);
        } catch (RepositoryException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    private void save(Order order) throws ServiceException {
        try (RepositoryFactory factory = new RepositoryFactory()) {
            Repository<Order> repository = factory.getOrderRepository();
            repository.save(order);
        } catch (RepositoryException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

}
