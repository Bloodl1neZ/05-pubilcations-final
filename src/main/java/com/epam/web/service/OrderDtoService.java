package com.epam.web.service;

import com.epam.web.entity.Order;
import com.epam.web.entity.dto.OrderDto;
import com.epam.web.entity.dto.SubscriptionDto;
import com.epam.web.exception.ServiceException;

import java.util.List;
import java.util.Optional;

public class OrderDtoService {
    public OrderDto getOrderByOrderId(String orderId) throws ServiceException {
        OrderService orderService = new OrderService();
        Optional<Order> orderOptional = orderService.getOrderByOrderId(orderId);
        Order order = orderOptional.get();
        return getOrderDtoByOrder(order);
    }

    public OrderDto getCurrentOrderByUserId(int id) throws ServiceException {
        OrderService orderService = new OrderService();
        Order order = orderService.getCurrentOrderByUserId(id);
        return getOrderDtoByOrder(order);
    }

    private OrderDto getOrderDtoByOrder(Order order) throws ServiceException {
        SubscriptionDtoService subscriptionDtoService = new SubscriptionDtoService();
        int orderId = order.getId();
        List<SubscriptionDto> subscriptionDtos = subscriptionDtoService.getSubscriptionDtosByOrderId(orderId);
        OrderDto orderDto = new OrderDto(order, subscriptionDtos);
        return orderDto;
    }
}
