package com.epam.web.service;

import com.epam.web.entity.PublicationType;
import com.epam.web.entity.dto.PublicationDto;
import com.epam.web.exception.RepositoryException;
import com.epam.web.exception.ServiceException;
import com.epam.web.repository.Repository;
import com.epam.web.repository.factory.RepositoryFactory;
import com.epam.web.repository.specification.Specification;
import com.epam.web.repository.specification.dto.publication.AllPublicationDtoSpecification;
import com.epam.web.repository.specification.dto.publication.PublicationDtoByTitlePartSpecification;
import com.epam.web.repository.specification.dto.publication.PublicationDtoByTypeAndCostRangeSpecification;
import com.epam.web.repository.specification.dto.publication.PublicationDtoByTypeSpecification;
import com.epam.web.utils.StringUtils;

import java.util.List;

public class PublicationDtoService {
    private static final double MAX_COST_VALUE = 9999;

    public List<PublicationDto> getPublicationsByTypeAndCost(String publicationType,
                                                             String minCost, String maxCost) throws ServiceException {
        String publicationTypeUpper = publicationType.toUpperCase();
        PublicationType publicationTypeValue = PublicationType.valueOf(publicationTypeUpper);

        double minCostValue;
        if (StringUtils.isNullOrEmpty(minCost)) {
            minCostValue = 0;
        } else {
            minCostValue = Double.parseDouble(minCost);
        }

        double maxCostValue;
        if (StringUtils.isNullOrEmpty(maxCost)) {
            maxCostValue = MAX_COST_VALUE;
        } else {
            maxCostValue = Double.parseDouble(maxCost);
        }

        return query(new PublicationDtoByTypeAndCostRangeSpecification(publicationTypeValue, minCostValue, maxCostValue));
    }

    public List<PublicationDto> getPublicationsByTitle(String title) throws ServiceException {
        return query(new PublicationDtoByTitlePartSpecification(title));
    }

    public List<PublicationDto> getPublicationsByType(String publicationType) throws ServiceException {
        String publicationTypeUpper = publicationType.toUpperCase();
        PublicationType publicationTypeValue = PublicationType.valueOf(publicationTypeUpper);
        return query(new PublicationDtoByTypeSpecification(publicationTypeValue));
    }

    public List<PublicationDto> getAllPublications() throws ServiceException {
        return query(new AllPublicationDtoSpecification());
    }

    private List<PublicationDto> query(Specification specification) throws ServiceException {
        try (RepositoryFactory factory = new RepositoryFactory()) {
            Repository<PublicationDto> repository = factory.getPublicationDtoRepository();
            return repository.query(specification);
        } catch (RepositoryException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
