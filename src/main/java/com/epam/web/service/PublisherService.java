package com.epam.web.service;

import com.epam.web.entity.Publisher;
import com.epam.web.exception.RepositoryException;
import com.epam.web.exception.ServiceException;
import com.epam.web.repository.Repository;
import com.epam.web.repository.factory.RepositoryFactory;
import com.epam.web.repository.specification.Specification;
import com.epam.web.repository.specification.publisher.AllPublishersSpecification;
import com.epam.web.repository.specification.publisher.PublisherByIdSpecification;

import java.util.List;
import java.util.Optional;

public class PublisherService {
    public List<Publisher> getAllPublishers() throws ServiceException {
        return query(new AllPublishersSpecification());
    }

    public void addPublisher(String company) throws ServiceException {
        Publisher publisher = new Publisher(company);
        save(publisher);
    }

    public Optional<Publisher> getPublisherById(String publisherId) throws ServiceException {
        int publisherIdValue = Integer.parseInt(publisherId);
        return queryForSingleResult(new PublisherByIdSpecification(publisherIdValue));
    }

    private List<Publisher> query(Specification specification) throws ServiceException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<Publisher> repository = repositoryFactory.getPublisherRepository();
            return repository.query(specification);
        } catch (RepositoryException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    private Optional<Publisher> queryForSingleResult(Specification specification) throws ServiceException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<Publisher> repository = repositoryFactory.getPublisherRepository();
            return repository.queryForSingleResult(specification);
        } catch (RepositoryException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    private void save(Publisher publisher) throws ServiceException {
        try(RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<Publisher> repository = repositoryFactory.getPublisherRepository();
            repository.save(publisher);
        } catch (RepositoryException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
