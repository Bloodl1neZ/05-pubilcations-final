package com.epam.web.service;

import com.epam.web.entity.Order;
import com.epam.web.entity.Publication;
import com.epam.web.entity.Subscription;
import com.epam.web.entity.dto.OrderDto;
import com.epam.web.entity.dto.SubscriptionDto;
import com.epam.web.exception.RepositoryException;
import com.epam.web.exception.ServiceException;
import com.epam.web.repository.Repository;
import com.epam.web.repository.factory.RepositoryFactory;
import com.epam.web.repository.specification.Specification;
import com.epam.web.repository.specification.subscription.SubscriptionByIdSpecification;
import com.epam.web.utils.MonthUtils;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

public class SubscriptionService {

    public void addSubscriptionByUserIdAndPublicationIdAndMonthAmount(int userId, String publicationId, String monthAmount) throws ServiceException {
        PublicationService publicationService = new PublicationService();
        Optional<Publication> publicationOptional = publicationService.getPublicationById(publicationId);
        int monthAmountValue = Integer.parseInt(monthAmount);

        if (publicationOptional.isPresent() && MonthUtils.AVAILABLE_MONTHS.contains(monthAmountValue)) {
            OrderService orderService = new OrderService();
            Order order = orderService.getCurrentOrderByUserId(userId);
            int orderId = order.getId();

            int publicationIdValue = Integer.parseInt(publicationId);
            Subscription subscription = new Subscription(orderId, publicationIdValue, monthAmountValue);
            save(subscription);
        }
    }

    public void changeMonthAmountOfSubscriptionBySubscriptionIdAndUserId(String subscriptionId, String monthAmount, int userId) throws ServiceException {
        int monthAmountValue = Integer.parseInt(monthAmount);
        Integer subscriptionIdValue = Integer.valueOf(subscriptionId);
        Optional<Subscription> subscriptionInOrderOptional = getSubscriptionFromCurrentOrderByUserAndSubscriptionId(subscriptionIdValue, userId);
        if (MonthUtils.AVAILABLE_MONTHS.contains(monthAmountValue) && subscriptionInOrderOptional.isPresent()) {
            Subscription subscription = subscriptionInOrderOptional.get();
            int orderId = subscription.getOrderId();
            int publicationId = subscription.getPublicationId();
            Subscription updatedSubscription = new Subscription(subscriptionIdValue, orderId, publicationId, monthAmountValue);
            save(updatedSubscription);
        }
    }

    public void deleteSubscriptionByIdAndUserId(String subscriptionId, int userId) throws ServiceException {
        int subscriptionIdValue = Integer.parseInt(subscriptionId);
        if (getSubscriptionFromCurrentOrderByUserAndSubscriptionId(subscriptionIdValue, userId).isPresent()) {
            Specification specification = new SubscriptionByIdSpecification(subscriptionIdValue);
            delete(specification);
        }
    }

    private Optional<Subscription> getSubscriptionFromCurrentOrderByUserAndSubscriptionId(int subscriptionId, int userId) throws ServiceException {
        OrderDtoService orderDtoService = new OrderDtoService();
        OrderDto orderDto = orderDtoService.getCurrentOrderByUserId(userId);
        List<SubscriptionDto> subscriptionsInOrder = orderDto.getSubscriptionDtos();
        Optional<Subscription> optionalSubscription = Optional.empty();
        for (SubscriptionDto subscriptionDto : subscriptionsInOrder) {
            Subscription subscription = subscriptionDto.getSubscription();
            if (subscription.getId() == subscriptionId) {
                optionalSubscription = Optional.of(subscription);
            }
        }
        return optionalSubscription;
    }

    private void save(Subscription subscription) throws ServiceException {
        try (RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<Subscription> repository = repositoryFactory.getSubscriptionRepository();
            repository.save(subscription);
        } catch (RepositoryException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }

    private void delete(Specification specification) throws ServiceException {
        try (RepositoryFactory repositoryFactory = new RepositoryFactory()) {
            Repository<Subscription> repository = repositoryFactory.getSubscriptionRepository();
            repository.delete(specification);
        } catch (RepositoryException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
