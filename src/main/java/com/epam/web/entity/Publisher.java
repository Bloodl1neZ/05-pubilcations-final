package com.epam.web.entity;

import java.util.Objects;

public class Publisher implements Entity {
    private static final long serialVersionUID = 6101578153943683445L;

    public static final String TABLE_NAME = "publisher";

    private static final String PREFIX = TABLE_NAME + ".";

    public static final String ID = PREFIX + "id";
    public static final String COMPANY = PREFIX + "company";

    private final Integer id;
    private final String company;

    public Publisher(Integer id, String company) {
        this.id = id;
        this.company = company;
    }

    public Publisher(String company) {
        this(null, company);
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public String getIdRowName() {
        return ID;
    }

    public String getCompany() {
        return company;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Publisher publisher = (Publisher) o;
        return Objects.equals(id, publisher.id) &&
                Objects.equals(company, publisher.company);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, company);
    }

}
