package com.epam.web.entity;


import java.math.BigDecimal;
import java.util.Objects;

public class Publication implements Entity {
    private static final long serialVersionUID = 1225260891918336042L;

    public static final String TABLE_NAME = "publication";

    private static final String PREFIX = TABLE_NAME + ".";

    public static final String ID = PREFIX + "id";
    public static final String TITLE = PREFIX + "title";
    public static final String PUBLISHER_ID = PREFIX + "publisher_id";
    public static final String COST_PER_MONTH = PREFIX + "cost_per_month";
    public static final String TYPE = PREFIX + "type";

    private final Integer id;
    private final String title;
    private final int publisherId;
    private final BigDecimal costPerMonth;
    private final PublicationType publicationType;

    public Publication(Integer id, String title, int publisherId, BigDecimal costPerMonth, PublicationType publicationType) {
        this.id = id;
        this.title = title;
        this.publisherId = publisherId;
        this.costPerMonth = costPerMonth;
        this.publicationType = publicationType;
    }

    public Publication(String title, int publisherId, BigDecimal costPerMonth, PublicationType publicationType) {
        this(null, title, publisherId, costPerMonth, publicationType);
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String getIdRowName() {
        return ID;
    }

    public String getTitle() {
        return title;
    }

    public int getPublisherId() {
        return publisherId;
    }

    public BigDecimal getCostPerMonth() {
        return costPerMonth;
    }

    public PublicationType getPublicationType() {
        return publicationType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Publication that = (Publication) o;
        return publisherId == that.publisherId &&
                Objects.equals(id, that.id) &&
                Objects.equals(title, that.title) &&
                Objects.equals(costPerMonth, that.costPerMonth) &&
                publicationType == that.publicationType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, publisherId, costPerMonth, publicationType);
    }
}
