package com.epam.web.entity;

import java.util.Objects;

public class User implements Entity {
    private static final long serialVersionUID = 5878527751866536028L;
    public static final String TABLE_NAME = "user";

    private static final String PREFIX = TABLE_NAME + ".";

    public static final String ID = PREFIX + "id";
    public static final String LOGIN = PREFIX + "login";
    public static final String PASSWORD = PREFIX + "password";
    public static final String NAME = PREFIX + "name";
    public static final String SURNAME = PREFIX + "surname";
    public static final String ROLE = PREFIX + "role";
    public static final String IS_ACTIVE = PREFIX + "is_active";

    private final Integer id;
    private final String name;
    private final String surname;
    private final String login;
    private final String password;
    private final Role role;
    private final boolean active;

    public User(Integer id, String login, String password, String name, String surname, Role role, boolean active) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.role = role;
        this.active = active;
    }

    public User(String login, String password, String name, String surname, Role role, boolean active) {
        this(null, login, password, name, surname, role, active);
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String getIdRowName() {
        return ID;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() {
        return role;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public boolean isActive() {
        return active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(name, user.name) &&
                Objects.equals(surname, user.surname) &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                active == user.active &&
                role == user.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, login, password, role, active);
    }
}