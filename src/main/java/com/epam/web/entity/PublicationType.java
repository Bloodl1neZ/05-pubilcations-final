package com.epam.web.entity;

public enum PublicationType {
    MAGAZINE("magazine"),
    NEWSPAPER("newspaper"),
    BOOK("book");

    private final String value;

    PublicationType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
