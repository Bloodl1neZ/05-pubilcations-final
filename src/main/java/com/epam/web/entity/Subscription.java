package com.epam.web.entity;

import java.util.Objects;

public class Subscription implements Entity {
    private static final long serialVersionUID = -4791658999368242745L;

    public static final String TABLE_NAME = "subscription";

    private static final String PREFIX = TABLE_NAME + ".";

    public static final String ID = PREFIX + "id";
    public static final String PUBLICATION_ID = PREFIX + "publication_id";
    public static final String ORDER_ID = PREFIX + "order_id";
    public static final String MONTH_AMOUNT = PREFIX + "month_amount";

    private final Integer id;
    private final int orderId;
    private final int publicationId;
    private final int monthAmount;

    public Subscription(Integer id, int orderId, int publicationId, int monthAmount) {
        this.id = id;
        this.orderId = orderId;
        this.publicationId = publicationId;
        this.monthAmount = monthAmount;
    }

    public Subscription(int orderId, int publicationId, int monthAmount) {
        this(null, orderId, publicationId, monthAmount);
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public String getIdRowName() {
        return ID;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getPublicationId() {
        return publicationId;
    }

    public int getMonthAmount() {
        return monthAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subscription that = (Subscription) o;
        return orderId == that.orderId &&
                publicationId == that.publicationId &&
                monthAmount == that.monthAmount &&
                Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, orderId, publicationId, monthAmount);
    }
}
