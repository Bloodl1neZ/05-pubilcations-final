package com.epam.web.entity.dto;

import com.epam.web.entity.Entity;
import com.epam.web.entity.Order;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

public class OrderDto implements Entity {
    private static final long serialVersionUID = 4794892490043903032L;

    private final Order order;
    private final List<SubscriptionDto> subscriptionDtos;

    public OrderDto(Order order, List<SubscriptionDto> subscriptionDtos) {
        this.order = order;
        this.subscriptionDtos = subscriptionDtos;
    }

    public Order getOrder() {
        return order;
    }

    public List<SubscriptionDto> getSubscriptionDtos() {
        return subscriptionDtos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderDto that = (OrderDto) o;
        return Objects.equals(order, that.order)
                && Objects.equals(subscriptionDtos, that.subscriptionDtos);
    }

    public BigDecimal calculateTotalCost() {
        BigDecimal totalCost = new BigDecimal(0);
        for (SubscriptionDto subscriptionDto : subscriptionDtos) {
            totalCost = totalCost.add(subscriptionDto.calculateCost());
        }
        return totalCost;
    }

    @Override
    public int hashCode() {
        return Objects.hash(order, subscriptionDtos);
    }

    @Override
    public Integer getId() {
        return order.getId();
    }

    @Override
    public String getIdRowName() {
        return Order.ID;
    }
}
