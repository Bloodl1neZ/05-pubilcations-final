package com.epam.web.entity.dto;

import com.epam.web.entity.Entity;
import com.epam.web.entity.Publication;
import com.epam.web.entity.Subscription;

import java.math.BigDecimal;
import java.util.Objects;

public class SubscriptionDto implements Entity {
    private static final long serialVersionUID = 3271348702019301141L;

    private final Subscription subscription;
    private final PublicationDto publicationDto;

    public SubscriptionDto(Subscription subscription, PublicationDto publicationDto) {
        this.subscription = subscription;
        this.publicationDto = publicationDto;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public PublicationDto getPublicationDto() {
        return publicationDto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubscriptionDto that = (SubscriptionDto) o;
        return Objects.equals(subscription, that.subscription) &&
                Objects.equals(publicationDto, that.publicationDto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subscription, publicationDto);
    }

    public BigDecimal calculateCost() {
        Publication publication = publicationDto.getPublication();
        BigDecimal costPerMonth = publication.getCostPerMonth();
        int monthAmount = subscription.getMonthAmount();
        BigDecimal monthAmountDecimal = BigDecimal.valueOf(monthAmount);
        return costPerMonth.multiply(monthAmountDecimal);
    }

    @Override
    public Integer getId() {
        return subscription.getId();
    }

    @Override
    public String getIdRowName() {
        return Subscription.ID;
    }
}
