package com.epam.web.entity.dto;

import com.epam.web.entity.Entity;
import com.epam.web.entity.Publication;
import com.epam.web.entity.Publisher;

import java.util.Objects;

public class PublicationDto implements Entity {
    private static final long serialVersionUID = 1010682217026198967L;

    private final Publication publication;
    private final Publisher publisher;

    public PublicationDto(Publication publication, Publisher publisher) {
        this.publication = publication;
        this.publisher = publisher;
    }

    public Publication getPublication() {
        return publication;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PublicationDto that = (PublicationDto) o;
        return Objects.equals(publication, that.publication) &&
                Objects.equals(publisher, that.publisher);
    }

    @Override
    public int hashCode() {
        return Objects.hash(publication, publisher);
    }

    @Override
    public Integer getId() {
        return publication.getId();
    }

    @Override
    public String getIdRowName() {
        return Publication.ID;
    }
}
