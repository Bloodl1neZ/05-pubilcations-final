package com.epam.web.entity;

import java.util.Objects;

public class Order implements Entity {
    private static final long serialVersionUID = 7330161235515293596L;

    public static final String TABLE_NAME = "`order`";

    public static final String ID = "id";
    public static final String USER_ID = "user_id";
    public static final String PAID = "paid";

    private final Integer id;
    private final boolean paid;
    private final int userId;

    public Order(Integer id, int userId, boolean paid) {
        this.id = id;
        this.paid = paid;
        this.userId = userId;
    }

    public Order(int userId, boolean paid) {
        this(null, userId, paid);
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public String getIdRowName() {
        return ID;
    }

    public boolean isPaid() {
        return paid;
    }

    public int getUserId() {
        return userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return paid == order.paid &&
                userId == order.userId &&
                Objects.equals(id, order.id);
    }

    @Override
    public int hashCode() {
        int paidSQL = paid ? 1 : 0;
        return Objects.hash(id, paidSQL, userId);
    }
}
