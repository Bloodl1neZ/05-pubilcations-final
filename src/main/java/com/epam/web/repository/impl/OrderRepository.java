package com.epam.web.repository.impl;

import com.epam.web.builder.Builder;
import com.epam.web.builder.OrderBuilder;
import com.epam.web.entity.Order;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

public class OrderRepository extends AbstractRepository<Order> {

    public OrderRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected Builder<Order> getBuilder() {
        return new OrderBuilder();
    }

    @Override
    protected String getTableName() {
        return Order.TABLE_NAME;
    }

    @Override
    protected Map<String, Object> getFields(Order order) {
        Map<String, Object> fields = new HashMap<>();
        fields.put(Order.ID, order.getId());
        fields.put(Order.USER_ID, order.getUserId());
        fields.put(Order.PAID, order.isPaid() ? 1 : 0);
        return fields;
    }
}
