package com.epam.web.repository.impl;

import com.epam.web.builder.Builder;
import com.epam.web.builder.SubscriptionDtoBuilder;
import com.epam.web.entity.Subscription;
import com.epam.web.entity.dto.SubscriptionDto;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

public class SubscriptionDtoRepository extends AbstractRepository<SubscriptionDto> {

    public SubscriptionDtoRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected Builder<SubscriptionDto> getBuilder() {
        return new SubscriptionDtoBuilder();
    }

    @Override
    protected String getTableName() {
        return Subscription.TABLE_NAME;
    }

    @Override
    protected Map<String, Object> getFields(SubscriptionDto subscriptionDto) {
        Map<String, Object> fields = new HashMap<>();
        Subscription subscription = subscriptionDto.getSubscription();
        fields.put(Subscription.ID, subscription.getId());
        fields.put(Subscription.MONTH_AMOUNT, subscription.getMonthAmount());
        fields.put(Subscription.ORDER_ID, subscription.getOrderId());
        fields.put(Subscription.PUBLICATION_ID, subscription.getPublicationId());
        return fields;
    }
}
