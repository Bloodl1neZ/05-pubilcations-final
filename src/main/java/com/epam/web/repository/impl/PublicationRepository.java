package com.epam.web.repository.impl;

import com.epam.web.builder.Builder;
import com.epam.web.builder.PublicationBuilder;
import com.epam.web.entity.Publication;
import com.epam.web.entity.PublicationType;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

public class PublicationRepository extends AbstractRepository<Publication> {

    public PublicationRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected Builder<Publication> getBuilder() {
        return new PublicationBuilder();
    }

    @Override
    protected String getTableName() {
        return Publication.TABLE_NAME;
    }

    @Override
    protected Map<String, Object> getFields(Publication publication) {
        Map<String, Object> fields = new HashMap<>();
        fields.put(Publication.ID, publication.getId());
        fields.put(Publication.TITLE, publication.getTitle());
        fields.put(Publication.PUBLISHER_ID, publication.getPublisherId());
        fields.put(Publication.COST_PER_MONTH, publication.getCostPerMonth());
        PublicationType publicationType = publication.getPublicationType();
        fields.put(Publication.TYPE, publicationType.getValue());
        return fields;
    }
}
