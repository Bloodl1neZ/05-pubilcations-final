package com.epam.web.repository.impl;

import com.epam.web.builder.Builder;
import com.epam.web.entity.Entity;
import com.epam.web.exception.BuilderException;
import com.epam.web.exception.RepositoryException;
import com.epam.web.repository.Repository;
import com.epam.web.repository.helper.QueryHelper;
import com.epam.web.repository.specification.Specification;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractRepository<T extends Entity> implements Repository<T> {
    private static final String SELECT_QUERY = "SELECT * FROM ";
    private static final String DELETE_QUERY = "DELETE FROM ";

    private Connection connection;

    public AbstractRepository(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Optional<T> queryForSingleResult(Specification specification) throws RepositoryException {
        List<T> list = query(specification);
        if(list.size() == 1) {
            T value = list.get(0);
            return Optional.of(value);
        }
        return Optional.empty();
    }

    @Override
    public void delete(Specification specification) throws RepositoryException {
        String sql = specification.toSql();
        String query = DELETE_QUERY + getTableName() + " " + sql;
        List<Object> parameters = specification.getParameters();
        executeDelete(query, parameters);
    }

    private void executeDelete(String query, List<Object> parameters) throws RepositoryException {
        try {
            PreparedStatement preparedStatement = prepareStatementForDeleteOrSelectQuery(query, parameters);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RepositoryException(e.getMessage(), e);
        }
    }

    @Override
    public List<T> query(Specification specification) throws RepositoryException {
        String sql = specification.toSql();
        String query = SELECT_QUERY + getTableName() + " " + sql;
        List<Object> parameters = specification.getParameters();
        return executeQuery(query, parameters);
    }

    private List<T> executeQuery(String query, List<Object> parameters) throws RepositoryException {
        List<T> objects = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = prepareStatementForDeleteOrSelectQuery(query, parameters);
            ResultSet resultSet = preparedStatement.executeQuery();
            Builder<T> builder = getBuilder();
            while (resultSet.next()) {
                T item = builder.build(resultSet);
                objects.add(item);
            }
        } catch (SQLException | BuilderException e) {
            throw new RepositoryException(e.getMessage(), e);
        }

        return objects;
    }

    private PreparedStatement prepareStatementForDeleteOrSelectQuery(String query, List<Object> parameters) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        int length = parameters.size();
        for (int i = 0; i < length; i++) {
            preparedStatement.setString(i + 1, String.valueOf(parameters.get(i)));
        }
        return preparedStatement;
    }

    @Override
    public void save(T object) throws RepositoryException {
        String sql;
        QueryHelper<T> queryHelper = new QueryHelper<>();
        Map<String, Object> fields = getFields(object);
        if (object.getId() == null) {
            sql = queryHelper.makeInsertQuery(fields, getTableName(), object);
        } else {
            sql = queryHelper.makeUpdateQuery(fields, getTableName(), object);
        }
        executeSave(sql, fields, object);
    }

    private void executeSave(String query, Map<String, Object> fields, T object) throws RepositoryException {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            int i = 1;
            for (Map.Entry<String, Object> entry : fields.entrySet()) {
                Object value = entry.getValue();
                String key = entry.getKey();
                if (!key.equals(object.getIdRowName())) {
                    preparedStatement.setString(i++, String.valueOf(value));
                }
            }
            Object id = fields.get(object.getIdRowName());
            if (id != null) {
                preparedStatement.setString(i++, String.valueOf(id));
            }
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RepositoryException(e.getMessage(), e);
        }
    }

    protected abstract Builder<T> getBuilder();

    protected abstract String getTableName();

    protected abstract Map<String, Object> getFields(T obj);


}
