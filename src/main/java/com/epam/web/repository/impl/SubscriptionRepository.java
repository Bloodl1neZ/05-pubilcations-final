package com.epam.web.repository.impl;

import com.epam.web.builder.Builder;
import com.epam.web.builder.SubscriptionBuilder;
import com.epam.web.entity.Subscription;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

public class SubscriptionRepository extends AbstractRepository<Subscription> {
    public SubscriptionRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected Builder<Subscription> getBuilder() {
        return new SubscriptionBuilder();
    }

    @Override
    protected String getTableName() {
        return Subscription.TABLE_NAME;
    }

    @Override
    protected Map<String, Object> getFields(Subscription subscription) {
        Map<String, Object> fields = new HashMap<>();
        fields.put(Subscription.ID, subscription.getId());
        fields.put(Subscription.MONTH_AMOUNT, subscription.getMonthAmount());
        fields.put(Subscription.ORDER_ID, subscription.getOrderId());
        fields.put(Subscription.PUBLICATION_ID, subscription.getPublicationId());
        return fields;
    }
}
