package com.epam.web.repository.impl;

import com.epam.web.builder.Builder;
import com.epam.web.builder.PublisherBuilder;
import com.epam.web.entity.Publisher;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

public class PublisherRepository extends AbstractRepository<Publisher> {
    public PublisherRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected Builder<Publisher> getBuilder() {
        return new PublisherBuilder();
    }

    @Override
    protected String getTableName() {
        return Publisher.TABLE_NAME;
    }

    @Override
    protected Map<String, Object> getFields(Publisher publisher) {
        Map<String, Object> fields = new HashMap<>();
        fields.put(Publisher.ID, publisher.getId());
        fields.put(Publisher.COMPANY, publisher.getCompany());
        return fields;
    }
}
