package com.epam.web.repository.impl;

import com.epam.web.builder.Builder;
import com.epam.web.builder.UserBuilder;
import com.epam.web.entity.Role;
import com.epam.web.entity.User;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

public class UserRepository extends AbstractRepository<User> {

    public UserRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected Builder<User> getBuilder() {
        return new UserBuilder();
    }

    @Override
    protected String getTableName() {
        return User.TABLE_NAME;
    }

    @Override
    protected Map<String, Object> getFields(User user) {
        Map<String, Object> fields = new HashMap<>();
        fields.put(User.ID, user.getId());
        fields.put(User.LOGIN, user.getLogin());
        fields.put(User.PASSWORD, user.getPassword());
        fields.put(User.NAME, user.getName());
        fields.put(User.SURNAME, user.getSurname());
        Role role = user.getRole();
        fields.put(User.ROLE, role.getValue());
        fields.put(User.IS_ACTIVE, user.isActive() ? 1 : 0);
        return fields;
    }
}
