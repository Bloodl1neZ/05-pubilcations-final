package com.epam.web.repository.impl;

import com.epam.web.builder.Builder;
import com.epam.web.builder.PublicationDtoBuilder;
import com.epam.web.entity.Publication;
import com.epam.web.entity.PublicationType;
import com.epam.web.entity.Publisher;
import com.epam.web.entity.dto.PublicationDto;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

public class PublicationDtoRepository extends AbstractRepository<PublicationDto> {
    private static final String PUBLICATION_PREFIX = Publication.TABLE_NAME + ".";
    private static final String PUBLISHER_PREFIX = Publisher.TABLE_NAME + ".";


    public PublicationDtoRepository(Connection connection) {
        super(connection);
    }

    @Override
    protected Builder<PublicationDto> getBuilder() {
        return new PublicationDtoBuilder();
    }

    @Override
    protected String getTableName() {
        return Publication.TABLE_NAME;
    }

    @Override
    protected Map<String, Object> getFields(PublicationDto publicationDto) {
        Map<String, Object> fields = new HashMap<>();

        Publication publication = publicationDto.getPublication();
        fields.put(Publication.ID, publication.getId());
        fields.put(Publication.TITLE, publication.getTitle());
        fields.put(Publication.PUBLISHER_ID, publication.getPublisherId());
        fields.put(Publication.COST_PER_MONTH, publication.getCostPerMonth());
        PublicationType publicationType = publication.getPublicationType();
        fields.put(Publication.TYPE, publicationType.getValue());

        return fields;
    }
}
