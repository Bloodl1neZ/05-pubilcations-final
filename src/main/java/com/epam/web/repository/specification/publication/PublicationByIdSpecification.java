package com.epam.web.repository.specification.publication;

import com.epam.web.entity.Publication;
import com.epam.web.repository.specification.Specification;

import java.util.Arrays;
import java.util.List;

public class PublicationByIdSpecification implements Specification {
    private final int id;

    public PublicationByIdSpecification(int id) {
        this.id = id;
    }

    @Override
    public String toSql() {
        return "WHERE " + Publication.ID + " = ?;";
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(id);
    }
}
