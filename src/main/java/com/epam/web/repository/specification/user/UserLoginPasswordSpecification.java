package com.epam.web.repository.specification.user;

import com.epam.web.entity.User;
import com.epam.web.repository.specification.Specification;

import java.util.Arrays;
import java.util.List;

public class UserLoginPasswordSpecification implements Specification {
    private final String login;
    private final String password;

    public UserLoginPasswordSpecification(String login, String password) {
        this.login = login;
        this.password = password;
    }

    @Override
    public String toSql() {
        return " WHERE " + User.LOGIN + " = ? AND "+ User.PASSWORD + "= ?;";
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(login, password);
    }
}
