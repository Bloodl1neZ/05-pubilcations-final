package com.epam.web.repository.specification.user;

import com.epam.web.repository.specification.Specification;

import java.util.Collections;
import java.util.List;

public class AllUsersSpecification implements Specification {
    @Override
    public String toSql() {
        return ";";
    }

    @Override
    public List<Object> getParameters() {
        return Collections.emptyList();
    }
}
