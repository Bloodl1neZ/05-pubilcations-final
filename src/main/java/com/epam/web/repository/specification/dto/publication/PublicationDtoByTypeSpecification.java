package com.epam.web.repository.specification.dto.publication;

import com.epam.web.entity.Publication;
import com.epam.web.entity.PublicationType;
import com.epam.web.entity.Publisher;
import com.epam.web.repository.specification.Specification;

import java.util.Arrays;
import java.util.List;

public class PublicationDtoByTypeSpecification implements Specification {

    private final PublicationType publicationType;

    public PublicationDtoByTypeSpecification(PublicationType publicationType) {
        this.publicationType = publicationType;
    }

    @Override
    public String toSql() {
        return "INNER JOIN " + Publisher.TABLE_NAME + " ON " + Publication.PUBLISHER_ID
                + " = " + Publisher.ID + " WHERE "
                + Publication.TYPE + " = ?;";
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(publicationType.getValue());
    }
}
