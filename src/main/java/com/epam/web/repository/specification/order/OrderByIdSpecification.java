package com.epam.web.repository.specification.order;

import com.epam.web.entity.Order;
import com.epam.web.repository.specification.Specification;

import java.util.Arrays;
import java.util.List;

public class OrderByIdSpecification implements Specification {
    private final int id;

    public OrderByIdSpecification(int id) {
        this.id = id;
    }

    @Override
    public String toSql() {
        return "WHERE " + Order.ID + " = ?;";
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(id);
    }
}
