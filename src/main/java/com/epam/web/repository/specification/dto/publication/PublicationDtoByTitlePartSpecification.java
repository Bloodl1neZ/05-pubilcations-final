package com.epam.web.repository.specification.dto.publication;

import com.epam.web.entity.Publication;
import com.epam.web.entity.Publisher;
import com.epam.web.repository.specification.Specification;

import java.util.Arrays;
import java.util.List;

public class PublicationDtoByTitlePartSpecification implements Specification {

    private final String titlePart;

    public PublicationDtoByTitlePartSpecification(String titlePart) {
        this.titlePart = '%' + titlePart + '%';
    }

    @Override
    public String toSql() {
        return "INNER JOIN " + Publisher.TABLE_NAME + " ON " + Publication.PUBLISHER_ID
                + " = " + Publisher.ID + " WHERE "
                + Publication.TITLE + " LIKE ?;";
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(titlePart);
    }
}
