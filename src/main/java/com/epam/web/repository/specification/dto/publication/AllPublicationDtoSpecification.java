package com.epam.web.repository.specification.dto.publication;

import com.epam.web.entity.Publication;
import com.epam.web.entity.Publisher;
import com.epam.web.repository.specification.Specification;

import java.util.Collections;
import java.util.List;

public class AllPublicationDtoSpecification implements Specification {

    @Override
    public String toSql() {
        return "INNER JOIN " + Publisher.TABLE_NAME + " ON " + Publication.PUBLISHER_ID
                + " = " + Publisher.ID + ";";
    }

    @Override
    public List<Object> getParameters() {
        return Collections.emptyList();
    }
}
