package com.epam.web.repository.specification.publisher;

import com.epam.web.entity.Publisher;
import com.epam.web.repository.specification.Specification;

import java.util.Arrays;
import java.util.List;

public class PublisherByIdSpecification implements Specification {
    private final int id;

    public PublisherByIdSpecification(int id) {
        this.id = id;
    }

    @Override
    public String toSql() {
        return "WHERE " + Publisher.ID + " = ?;";
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(id);
    }

}
