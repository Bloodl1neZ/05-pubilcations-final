package com.epam.web.repository.specification.dto.publication;

import com.epam.web.entity.Publication;
import com.epam.web.entity.PublicationType;
import com.epam.web.entity.Publisher;
import com.epam.web.repository.specification.Specification;

import java.util.Arrays;
import java.util.List;

public class PublicationDtoByTypeAndCostRangeSpecification implements Specification {

    private final PublicationType publicationType;
    private final double minCost;
    private final double maxCost;

    public PublicationDtoByTypeAndCostRangeSpecification(PublicationType publicationType, double minCost, double maxCost) {
        this.publicationType = publicationType;
        this.minCost = minCost;
        this.maxCost = maxCost;
    }

    @Override
    public String toSql() {
        return "INNER JOIN " + Publisher.TABLE_NAME + " ON " + Publication.PUBLISHER_ID
                + " = " + Publisher.ID + " WHERE "
                + Publication.TYPE + " = ? AND "
                + Publication.COST_PER_MONTH + " >= ? AND "
                + Publication.COST_PER_MONTH + " <= ?;";
    }


    @Override
    public List<Object> getParameters() {
        return Arrays.asList(publicationType.getValue(), minCost, maxCost);
    }
}
