package com.epam.web.repository.specification.order;

import com.epam.web.entity.Order;
import com.epam.web.repository.specification.Specification;

import java.util.Arrays;
import java.util.List;

public class OrdersByUserIdAndPaidSpecification implements Specification {
    private final int userId;
    private final boolean paid;

    public OrdersByUserIdAndPaidSpecification(int userId, boolean paid) {
        this.userId = userId;
        this.paid = paid;
    }

    @Override
    public String toSql() {
        return "WHERE " + Order.USER_ID + " = ? and " + Order.PAID + " = ?;";
    }

    @Override
    public List<Object> getParameters() {
        int paidSql = paid ? 1 : 0;
        return Arrays.asList(userId, paidSql);
    }
}
