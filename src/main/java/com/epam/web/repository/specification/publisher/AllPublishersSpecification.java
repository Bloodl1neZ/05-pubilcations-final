package com.epam.web.repository.specification.publisher;

import com.epam.web.repository.specification.Specification;

import java.util.Collections;
import java.util.List;

public class AllPublishersSpecification implements Specification {
    @Override
    public String toSql() {
        return ";";
    }

    @Override
    public List<Object> getParameters() {
        return Collections.emptyList();
    }
}
