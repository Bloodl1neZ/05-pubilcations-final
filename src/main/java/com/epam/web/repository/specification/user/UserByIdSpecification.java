package com.epam.web.repository.specification.user;

import com.epam.web.entity.User;
import com.epam.web.repository.specification.Specification;

import java.util.Arrays;
import java.util.List;

public class UserByIdSpecification implements Specification {
    private final int id;

    public UserByIdSpecification(int id) {
        this.id = id;
    }

    @Override
    public String toSql() {
        return " WHERE " + User.ID + " = ?;";
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(id);
    }


}
