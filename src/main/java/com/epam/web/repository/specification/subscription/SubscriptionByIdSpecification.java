package com.epam.web.repository.specification.subscription;

import com.epam.web.entity.Subscription;
import com.epam.web.repository.specification.Specification;

import java.util.Arrays;
import java.util.List;

public class SubscriptionByIdSpecification implements Specification {
    private final int id;

    public SubscriptionByIdSpecification(int id) {
        this.id = id;
    }

    @Override
    public String toSql() {
        return "WHERE " + Subscription.ID + " = ?;";
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(id);
    }
}
