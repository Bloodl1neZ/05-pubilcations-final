package com.epam.web.repository.specification.dto.subscription;

import com.epam.web.entity.Publication;
import com.epam.web.entity.Publisher;
import com.epam.web.entity.Subscription;
import com.epam.web.repository.specification.Specification;

import java.util.Arrays;
import java.util.List;

public class SubscriptionDtoByOrderIdSpecification implements Specification {
    private final int orderId;

    public SubscriptionDtoByOrderIdSpecification(int orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toSql() {
        return "INNER JOIN " + Publication.TABLE_NAME + " ON "
                + Subscription.PUBLICATION_ID + " = "
                + Publication.ID + " AND "
                + Subscription.ORDER_ID + " = ? INNER JOIN " + Publisher.TABLE_NAME
                + " ON " + Publisher.ID + " = "
                + Publication.PUBLISHER_ID + ";";
    }

    @Override
    public List<Object> getParameters() {
        return Arrays.asList(orderId);
    }
}
