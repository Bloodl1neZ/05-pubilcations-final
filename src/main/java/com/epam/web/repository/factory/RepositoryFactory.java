package com.epam.web.repository.factory;

import com.epam.web.connection.ConnectionPool;
import com.epam.web.entity.*;
import com.epam.web.entity.dto.PublicationDto;
import com.epam.web.entity.dto.SubscriptionDto;
import com.epam.web.repository.Repository;
import com.epam.web.repository.impl.*;

import java.sql.Connection;

public class RepositoryFactory implements AutoCloseable {
    private final Connection connection;

    public RepositoryFactory() {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        this.connection = connectionPool.getConnection();
    }

    public Repository<User> getUserRepository() {
        return new UserRepository(connection);
    }

    public Repository<Publication> getPublicationRepository() {
        return new PublicationRepository(connection);
    }

    public Repository<Subscription> getSubscriptionRepository() {
        return new SubscriptionRepository(connection);
    }

    public Repository<Order> getOrderRepository() {
        return new OrderRepository(connection);
    }

    public Repository<SubscriptionDto> getSubscriptionDtoRepository() {
        return new SubscriptionDtoRepository(connection);
    }

    public Repository<PublicationDto> getPublicationDtoRepository() {
        return new PublicationDtoRepository(connection);
    }

    public Repository<Publisher> getPublisherRepository() {
        return new PublisherRepository(connection);
    }

    @Override
    public void close() {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        connectionPool.releaseConnection(connection);
    }
}
