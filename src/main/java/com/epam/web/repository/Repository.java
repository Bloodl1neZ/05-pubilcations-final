package com.epam.web.repository;

import com.epam.web.exception.RepositoryException;
import com.epam.web.repository.specification.Specification;
import com.epam.web.entity.Entity;

import java.util.List;
import java.util.Optional;

/**
 * This interface is built on Repository pattern and it's medium level between database and service logic
 * @param <T> the type of objects with which implementation operates. Required implementation of {@code Entity} interface
 */
public interface Repository<T extends Entity> {
    List<T> query(Specification specification) throws RepositoryException;

    Optional<T> queryForSingleResult(Specification specification) throws RepositoryException;

    void save(T object) throws RepositoryException;

    void delete(Specification specification) throws RepositoryException;
}
