package com.epam.web.repository.helper;

import com.epam.web.entity.Entity;

import java.util.Map;

public class QueryHelper<T extends Entity> {
    private static final String INSERT_QUERY = "INSERT INTO ";
    private static final String VALUES = "VALUES";
    private static final String UPDATE_QUERY = "UPDATE ";
    private static final String SET = " SET ";

    public String makeInsertQuery(Map<String, Object> fields, String table, T object) {
        StringBuilder columns = new StringBuilder("(");
        StringBuilder values = new StringBuilder("(");

        for (Map.Entry<String, Object> entry : fields.entrySet()) {
            String column = entry.getKey();
            if (column.equals(object.getIdRowName())) {
                continue;
            }
            columns.append(column).append(", ");
            values.append("?, ");
        }

        values.deleteCharAt(values.lastIndexOf(","));
        columns.deleteCharAt(columns.lastIndexOf(","));
        values.append(")");
        columns.append(")");

        return INSERT_QUERY + table + columns + VALUES + values + ";";
    }

    public String makeUpdateQuery(Map<String, Object> fields, String table, T object) {
        StringBuilder where = new StringBuilder();
        StringBuilder set = new StringBuilder();
        for (Map.Entry<String, Object> entry : fields.entrySet()) {
            String column = entry.getKey();
            Object value = entry.getValue();
            if (value != null) {
                if(column.equals(object.getIdRowName())) {
                    where.append(" WHERE " + object.getIdRowName() + "=?");
                } else {
                    set.append(" ").append(column).append("=?,");
                }
            }
        }
        int lastIndexOfComma = set.lastIndexOf(",");
        set.deleteCharAt(lastIndexOfComma);
        return UPDATE_QUERY + table + SET + set + where + ";";
    }


}
