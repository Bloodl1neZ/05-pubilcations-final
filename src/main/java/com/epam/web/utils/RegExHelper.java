package com.epam.web.utils;

public class RegExHelper {
    public static final String ID_REGEX = "^[0-9]+$";
    public static final String MONEY_REGEX = "^[0-9]+((\\.|,)[0-9]{1,2})?$";
    public static final String TITLE_REGEX = "^[A-Za-z0-9',. ]{1,40}$";
    public static final String CREDIT_CARD_REGEX = "^[0-9]{16}$";
    public static final String NAME_REGEX = "^[A-Z ,.'-]+$";
}
