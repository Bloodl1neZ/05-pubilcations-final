package com.epam.web.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

    public static boolean isNullOrEmpty(String value) {
        return value == null || value.trim().isEmpty();
    }

    public static boolean isMatchesToRegEx(String value, String regEx, boolean nullable) {
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(value);
        if (nullable) {
            return isNullOrEmpty(value) || matcher.matches();
        }
        return !isNullOrEmpty(value) && matcher.matches();
    }
}
