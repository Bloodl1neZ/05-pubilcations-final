package com.epam.web.utils;

public enum Page {
    HOME("/WEB-INF/pages/main.jsp"),
    PUBLICATIONS("/WEB-INF/pages/publications.jsp"),
    ORDER("/WEB-INF/pages/order.jsp"),
    LOGIN("/WEB-INF/pages/login.jsp"),
    USERS("/WEB-INF/pages/users.jsp"),
    PUBLICATION_MANAGER("/WEB-INF/pages/publication_manager.jsp"),
    ERROR("/WEB-INF/pages/error.jsp");

    private final String value;

    Page(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
