package com.epam.web.connection;

import com.epam.web.exception.ConnectionException;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionCreator {
    private static final String DB_PROPERTIES = "config.properties";
    private static final String DB_HOST = "db.host";
    private static final String DB_LOGIN = "db.login";
    private static final String DB_PASSWORD = "db.password";

    private final String host;
    private final String login;
    private final String password;

    public ConnectionCreator() {
        ClassLoader classLoader = getClass().getClassLoader();

        try (InputStream inputStream = classLoader.getResourceAsStream(DB_PROPERTIES)) {
            Properties properties = new Properties();
            properties.load(inputStream);

            host = properties.getProperty(DB_HOST);
            login = properties.getProperty(DB_LOGIN);
            password = properties.getProperty(DB_PASSWORD);

            Driver driver = new com.mysql.jdbc.Driver();
            DriverManager.registerDriver(driver);
        } catch (SQLException | IOException e) {
            throw new ConnectionException(e.getMessage(), e);
        }
    }

    public Connection createConnection() {
        try {
            return DriverManager.getConnection(host, login, password);
        } catch (SQLException e) {
            throw new ConnectionException(e.getMessage(), e);
        }
    }
}
