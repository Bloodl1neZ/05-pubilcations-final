package com.epam.web.connection;

import com.epam.web.exception.ConnectionPoolException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {
    private static final AtomicBoolean initialized = new AtomicBoolean(false);
    private static final Lock CONNECTIONS_LOCK = new ReentrantLock();
    private static final Lock INSTANCE_LOCK = new ReentrantLock();

    private static final int POOL_SIZE = 10;

    private static ConnectionPool instance;

    private Queue<Connection> connections;
    private final Semaphore semaphore = new Semaphore(POOL_SIZE, true);
    private final ConnectionCreator connectionCreator = new ConnectionCreator();

    public static ConnectionPool getInstance() {
        if (!initialized.get()) {
            try {
                INSTANCE_LOCK.lock();
                if (!initialized.get()) {
                    instance = new ConnectionPool();
                    instance.init();
                }
            } finally {
                INSTANCE_LOCK.unlock();
            }
        }
        return instance;
    }

    private ConnectionPool() {
        connections = new ArrayDeque<>(POOL_SIZE);
    }

    private void init() {
        for (int i = 0; i < POOL_SIZE; i++) {
            Connection connection = connectionCreator.createConnection();
            connections.add(connection);
        }
        initialized.set(true);
    }

    public Connection getConnection() {
        try {
            semaphore.acquire();
            CONNECTIONS_LOCK.lock();
            return connections.poll();
        } catch (InterruptedException e) {
            throw new ConnectionPoolException(e.getMessage(), e);
        } finally {
            CONNECTIONS_LOCK.unlock();
        }
    }

    public boolean releaseConnection(Connection connection) {
        try {
            CONNECTIONS_LOCK.lock();
            boolean result = connections.add(connection);
            if (result) {
                semaphore.release();
            }
            return result;
        } finally {
            CONNECTIONS_LOCK.unlock();
        }
    }

    public void closeConnections() {
        try {
            for (Connection connection : connections) {
                connection.close();
            }
        } catch (SQLException e) {
            throw new ConnectionPoolException(e.getMessage(), e);
        }
    }
}
