package com.epam.web.builder;

import com.epam.web.entity.Entity;
import com.epam.web.exception.BuilderException;

import java.sql.ResultSet;

/**
 *
 * @param <T> is type of object which will be constructed by class implementing this {@code Builder}
 */
public interface Builder<T extends Entity> {
    /**
     *
     * @param resultSet is a result of query to database
     * @return an instance of {@code T} class
     * @throws BuilderException
     */
    T build(ResultSet resultSet) throws BuilderException;
}
