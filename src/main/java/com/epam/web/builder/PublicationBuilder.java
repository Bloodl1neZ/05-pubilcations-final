package com.epam.web.builder;

import com.epam.web.entity.Publication;
import com.epam.web.entity.PublicationType;
import com.epam.web.exception.BuilderException;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PublicationBuilder implements Builder<Publication> {

    @Override
    public Publication build(ResultSet resultSet) throws BuilderException {
        try {
            int id = resultSet.getInt(Publication.ID);
            String title = resultSet.getString(Publication.TITLE);
            String publisherId = resultSet.getString(Publication.PUBLISHER_ID);
            int publisherIdValue = Integer.parseInt(publisherId);
            BigDecimal cost = resultSet.getBigDecimal(Publication.COST_PER_MONTH);

            String type = resultSet.getString(Publication.TYPE);
            String typeUpper = type.toUpperCase();
            PublicationType publicationType = PublicationType.valueOf(typeUpper);
            return new Publication(id, title, publisherIdValue, cost, publicationType);
        } catch (SQLException e) {
            throw new BuilderException(e.getMessage(), e);
        }
    }
}
