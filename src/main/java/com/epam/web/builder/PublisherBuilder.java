package com.epam.web.builder;

import com.epam.web.entity.Publisher;
import com.epam.web.exception.BuilderException;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PublisherBuilder implements Builder<Publisher> {

    @Override
    public Publisher build(ResultSet resultSet) throws BuilderException {
        try {
            String company = resultSet.getString(Publisher.COMPANY);
            Integer id = resultSet.getInt(Publisher.ID);
            return new Publisher(id, company);
        } catch (SQLException e) {
            throw new BuilderException(e.getMessage(), e);
        }
    }
}
