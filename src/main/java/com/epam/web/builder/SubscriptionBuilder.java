package com.epam.web.builder;

import com.epam.web.entity.Subscription;
import com.epam.web.exception.BuilderException;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SubscriptionBuilder implements Builder<Subscription> {

    @Override
    public Subscription build(ResultSet resultSet) throws BuilderException {
        try {
            Integer id = resultSet.getInt(Subscription.ID);
            int publicationId = resultSet.getInt(Subscription.PUBLICATION_ID);
            int orderId = resultSet.getInt(Subscription.ORDER_ID);
            int monthAmount = resultSet.getInt(Subscription.MONTH_AMOUNT);
            return new Subscription(id, orderId, publicationId, monthAmount);
        } catch (SQLException e) {
            throw new BuilderException(e.getMessage(), e);
        }
    }
}
