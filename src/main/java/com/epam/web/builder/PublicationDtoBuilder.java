package com.epam.web.builder;

import com.epam.web.entity.Publication;
import com.epam.web.entity.Publisher;
import com.epam.web.entity.dto.PublicationDto;
import com.epam.web.exception.BuilderException;

import java.sql.ResultSet;

public class PublicationDtoBuilder implements Builder<PublicationDto> {
    @Override
    public PublicationDto build(ResultSet resultSet) throws BuilderException {
        Builder<Publication> publicationBuilder = new PublicationBuilder();
        Publication publication = publicationBuilder.build(resultSet);

        Builder<Publisher> publisherBuilder = new PublisherBuilder();
        Publisher publisher = publisherBuilder.build(resultSet);

        return new PublicationDto(publication, publisher);
    }
}
