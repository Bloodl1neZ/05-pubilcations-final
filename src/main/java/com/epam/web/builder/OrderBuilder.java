package com.epam.web.builder;

import com.epam.web.entity.Order;
import com.epam.web.exception.BuilderException;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderBuilder implements Builder<Order> {

    @Override
    public Order build(ResultSet resultSet) throws BuilderException {
        try {
            Integer id = resultSet.getInt(Order.ID);
            int userId = resultSet.getInt(Order.USER_ID);
            boolean paid = resultSet.getBoolean(Order.PAID);
            return new Order(id, userId, paid);
        } catch (SQLException e) {
            throw new BuilderException(e.getMessage(), e);
        }
    }
}
