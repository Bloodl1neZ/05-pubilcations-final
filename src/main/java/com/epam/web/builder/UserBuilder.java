package com.epam.web.builder;

import com.epam.web.entity.Role;
import com.epam.web.entity.User;
import com.epam.web.exception.BuilderException;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserBuilder implements Builder<User> {
    @Override
    public User build(ResultSet resultSet) throws BuilderException {
        try {
            Integer id = resultSet.getInt(User.ID);
            String name = resultSet.getString(User.NAME);
            String surname = resultSet.getString(User.SURNAME);
            String login = resultSet.getString(User.LOGIN);
            String password = resultSet.getString(User.PASSWORD);
            String role = resultSet.getString(User.ROLE);
            String roleUpper = role.toUpperCase();
            boolean active = resultSet.getBoolean(User.IS_ACTIVE);
            return new User(id, login, password, name, surname, Role.valueOf(roleUpper), active);
        } catch (SQLException e) {
            throw new BuilderException(e.getMessage(), e);
        }
    }
}
