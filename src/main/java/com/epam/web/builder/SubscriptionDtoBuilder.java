package com.epam.web.builder;

import com.epam.web.entity.Subscription;
import com.epam.web.entity.dto.PublicationDto;
import com.epam.web.entity.dto.SubscriptionDto;
import com.epam.web.exception.BuilderException;

import java.sql.ResultSet;

public class SubscriptionDtoBuilder implements Builder<SubscriptionDto> {

    @Override
    public SubscriptionDto build(ResultSet resultSet) throws BuilderException {
        SubscriptionBuilder subscriptionBuilder = new SubscriptionBuilder();
        Subscription subscription = subscriptionBuilder.build(resultSet);

        PublicationDtoBuilder publicationDtoBuilder = new PublicationDtoBuilder();
        PublicationDto publicationDto = publicationDtoBuilder.build(resultSet);

        return new SubscriptionDto(subscription, publicationDto);
    }
}
