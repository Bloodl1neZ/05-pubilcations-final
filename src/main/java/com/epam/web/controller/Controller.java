package com.epam.web.controller;

import com.epam.web.command.Command;
import com.epam.web.command.CommandResult;
import com.epam.web.command.factory.CommandFactory;
import com.epam.web.connection.ConnectionPool;
import com.epam.web.exception.CommandException;
import com.epam.web.utils.Page;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Controller extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);
    private static final String COMMAND_REQUEST_PARAMETER = "command";

    private static final String ERROR = "error";

    public void init() {
        try {
            super.init();
        } catch (ServletException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        process(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        CommandFactory commandFactory = new CommandFactory();
        String parameter = request.getParameter(COMMAND_REQUEST_PARAMETER);
        Command command = commandFactory.create(parameter);
        CommandResult commandResult;

        try {
            commandResult = command.execute(request, response);
        } catch (CommandException e) {
            LOGGER.error(e.getMessage(), e);
            request.setAttribute(ERROR, e.getMessage());
            commandResult = CommandResult.forward(Page.ERROR.getValue());
        }

        String page = commandResult.getPage();
        if (commandResult.isRedirect()) {
            response.sendRedirect(page);
        } else {
            RequestDispatcher requestDispatcher = request.getRequestDispatcher(page);
            requestDispatcher.forward(request, response);
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        connectionPool.closeConnections();
    }
}
