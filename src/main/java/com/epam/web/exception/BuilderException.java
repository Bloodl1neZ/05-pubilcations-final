package com.epam.web.exception;

public class BuilderException extends Exception {
    public  BuilderException(String message, Throwable cause) {
        super(message, cause);
    }
}
