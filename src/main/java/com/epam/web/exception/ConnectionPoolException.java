package com.epam.web.exception;

public class ConnectionPoolException extends RuntimeException {
    public  ConnectionPoolException(String message, Throwable cause) {
        super(message, cause);
    }
}
