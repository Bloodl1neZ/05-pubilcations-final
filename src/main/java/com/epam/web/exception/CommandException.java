package com.epam.web.exception;

public class CommandException extends Exception {
    public CommandException(String message, Throwable cause) {
        super(message, cause);
    }
}
